
## How to install the dev environment

This repo has scripts to automatically install for ubuntu 18.04 and debian 10

1. `./install.sh`

2. `source ~/.bashrc`

3. `./install2.sh`

### Test
To run the unit tests
- `composer test`

### Demo
Local Frontend:
1. `cd endpoints/react`
2. `npm install`
3. `npm start`

Heroku:
    URL Coming soon

### How to build with docker
You can use docker to build the application and access it at port 9000

1. `docker-compose up`

## Workflow
1. Any time you create a new php file, remember to run `composer du`

2. 
    - Package Tests go in `endpoints/tests`. 
        - Create a new class that extends `\PHPUnit\Framework\TestCase` in `endpoints/tests/tests/domain/`
    - Frontend code goes in `react`

3. To run the unit tests,
    ```
    composer test -- # args
    ```

Say you want to create a new `UseCase`

- Create the new class in `packages/domain/UseCases/`

Say you want to create a new `Entity`

- Create the new class in `packages/domain/Entities/`

Say you want to query the database, e.g. in your `UseCase`

Create a repo class in packages/domain/Repositories:
    
   ```php
    class PartyRepo
    {

        private Persister $saver;

        function __construct(Persister $saver)
        {
            $this->saver = $saver;
        }

        function saveNew(array $req) {
            $this->saver->save("party",$req);
        }

        function getById($id):Party {
            return $this->saver->get("party",$id);
        }

    }
   ```

   ```php
    class NewParty
    {

        private $partyRepo;

        public function __construct(PartyRepo $partyRepo)
        {
            $this->partyRepo = $partyRepo;
        }

    }
   ```

## Things you shouldn't do:
- Don't `new` up a class that is marked `@internal` in another package.
    
    - The only exceptions being in a DI container or using `InMemory`


## Directory Breakdown

### Scripts
`scripts` is where any top level scripts go. They are
also used to interact with any `endpoints`, such as 
creating files

You do **not** need to change directory in order to
interact with the endpoints.

To interact with `artisan` in the `laravelApp` endpoint:

```
composer artisan -- make:controller HelloController
```

To interact with the tests in the `tests` endpoint:
```
composer test -- # args
```

### Endpoints

`endpoints` are responsible for doing two things:

- Resolving what implementation classes to use
for any interfaces in `packages`.
These will then be injected in their framework specific containers,
such as [Laravel Service Providers](https://laravel.com/docs/6.x/providers) or [Wordpress Plugins](https://www.smashingmagazine.com/2011/09/how-to-create-a-wordpress-plugin/)

- Calling the `packages/domain/UseCases`

### Packages
Any personal packages that we develop go here. 

Why do all of them have a `composer.json`?

This is in case we want to migrate these packages to other
git repos and track versions separately.


> _The granule of **reuse** is the granule of **release**._

> _Only components that are released through a tracking system can be effectively reused._

> _The granule is the package._
>> The “Granularity” paper by Robert C. Martin (Uncle Bob)

#### packages/domain
In here:

- No web servers
- No databases
- No network calls
- No logging

is allowed.

#### packages/persitance
An example of how to persist data. These are used by repositories

Persisters are an instance of the [Gateway Pattern](https://martinfowler.com/eaaCatalog/gateway.html)

The difference between `Persisters` and `Repository`:

- Persisters may interact with an api, regardless of the `$resource`
- Repositories are **`$resource`-specific**

This is why `packages/persistance` is allowed to depend on `doctrine/dbal` as well as the mongo extension