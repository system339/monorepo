#!/usr/bin/env bash
cd ./endpoints
for dir in $(ls -d -- */); do
  echo $dir
  cd $dir
  if test -f "composer.json"; then
    composer install
    header "$dir's" dependencies were installed!
  else
    header No composer.json found for $dir
  fi
  cd ../
done;
cd ../
composer install