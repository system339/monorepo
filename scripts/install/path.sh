#!/usr/bin/env bash

cat <<-EOF >> $HOME/.bashrc

BIN_DIR="\$HOME/bin/"
VENDOR_BIN="\$HOME/.config/composer/vendor/bin"

export PATH="\$PATH:\$BIN_DIR:\$VENDOR_BIN"
EOF

