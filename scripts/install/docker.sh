#!/usr/bin/env bash
sudo apt remove -y docker docker-engine docker.io containerd runc
sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

cat /etc/issue | grep Ubuntu && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
cat /etc/issue | grep Ubuntu && sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

cat /etc/issue | grep Debian && curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
cat /etc/issue | grep Debian && sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"

sudo apt-get install docker-ce docker-ce-cli containerd.io

sudo curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose