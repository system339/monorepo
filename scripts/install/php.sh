#!/usr/bin/env bash

while true
do
  echo "Ubuntu or Debian 10? ANSWER u or d"
  read -r ANSWER

  if [[ $ANSWER = "u" ]]
  then
    echo "Installing for Ubuntu"
    sudo add-apt-repository ppa:ondrej/php
    sudo apt install php-{bcmath,curl,gd,imagick,mbstring,mysql,soap,xml,zip,xdebug,ds}
    exit
  elif [[ $ANSWER = "d" ]]
  then
    echo "Installing for Debian 10 buster"
    wget -q https://packages.sury.org/php/apt.gpg -O- | sudo apt-key add -
    sudo echo "deb https://packages.sury.org/php/ buster main" | sudo tee /etc/apt/sources.list.d/php.list
    sudo apt update
    sudo apt install php-{fpm,bcmath,curl,gd,imagick,mbstring,mysql,soap,xml,zip,json,xdebug,ds}
    exit
  fi
done
