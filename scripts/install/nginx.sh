#!/usr/bin/env bash

while true
do
  echo "Ubuntu or Debian 10? ANSWER u or d"
  read -r ANSWER

  if [[ $ANSWER == "u" ]]
  then
      echo "Installing for Ubuntu"
      sudo add-apt-repository ppa:nginx/stable
      sudo apt-get purge apache2 apache2-utils apache2.2-bin apache2-common -y
      sudo service nginx stop
      sudo apt install nginx -y
      exit
  elif [[ $ANSWER == "d" ]]
  then
      echo "Installing for Debian 10 buster"
      sudo apt install nginx -y
      exit
  fi
done
