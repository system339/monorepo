#!/usr/bin/env bash
sudo apt install network-manager libnss3-tools jq xsel php7.1-mcrypt php-{common,cli,curl,mbstring,xml,zip,sqlite3,mysql,mongodb} -y
composer global require cpriego/valet-linux
valet || echo "Please stop the script"
valet install