<?php

use System\Domain\Builders\FoodTypeBuilder;
use System\Domain\Builders\LineItemBuilder;

describe("LineItemBuilder", function () {
   it("takes quantity,barcode,name,expirationDate,foodType",function() {
       $quantity = 1;
       $barcode = 12345678901234;
       $name = "Body Gear";
       $expirationDate = "01/01/2000";

       $foodType = (new FoodTypeBuilder())
           ->foodType("NonPerishableFood")
           ->name("Pasta")
           ->build();

       $lineItem = (new LineItemBuilder())->
           quantity($quantity)->
           barcode($barcode)->
           name($name)->
           expirationDate($expirationDate)->
           type($foodType)->
           build();

       expect(get_object_vars($lineItem))->toBe([
           "quantity"=>$quantity,
           "barcode"=>$barcode,
           "name"=>$name,
           "expiration_date"=>$expirationDate,
           "foodType"=>$foodType
       ]);
   });

   xit("throws if quantity is not an integer",  function() {
       expect(function () {
           (new LineItemBuilder())->
                quantity(1.23);
       })->toThrow(new \Exception("quantity is not an integer"));
   });
});