<?php

namespace System\Tests;

use System\Domain\Builders\FieldRequiredException;
use System\Domain\Builders\FoodTypeBuilder;

describe('FoodTypeBuilder', function () {

    beforeEach(function() {
        $this->builder = new FoodTypeBuilder();

    });

    function expectedFoodTypeProperties($foodType, $expectedType, $expectedName){
        expect($foodType->type)->toBe($expectedType);
        expect($foodType->name)->toBe($expectedName);
    }

    it("takes a food type and name", function () {
        $foodType = $this->builder
            ->foodType("NonPerishableFood")
            ->name("Pasta")
            ->build();

//        expect($foodType)->toBe(["type" => "NonPerishableFood", "name" => "Pasta"]);
        expectedFoodTypeProperties($foodType, "NonPerishableFood",  "Pasta");
    });

    it("type must be either NonPerishableFood or NonFoodItem", function() {
        $foodType = $this->builder
            ->foodType("NonPerishableFood")
            ->name("Pasta")
            ->build();
        expectedFoodTypeProperties($foodType, "NonPerishableFood",  "Pasta");

        $foodType = $this->builder
            ->foodType("NonFoodItem")
            ->name("Pasta")
            ->build();
        expectedFoodTypeProperties($foodType, "NonFoodItem",  "Pasta");

        expect(function() {
            $this->builder->foodType("SomeOtherType");
        })->toThrow("Invalid food type: must be either 'NonPerishableFood' or 'NonFood'");
    });

    it("donor name required",function () {
        expect(function() {
            $this->builder->name("");
        })->toThrow(new FieldRequiredException);
    });

});
