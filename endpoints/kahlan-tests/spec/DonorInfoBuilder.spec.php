<?php

namespace System\Tests;

use System\Domain\Builders\FieldRequiredException;
use System\Domain\Builders\DonorInfoBuilder;
use System\Domain\UseCases\DonationManagement\Requests\DonorInfo;


function expectAccessFieldsAsObjectsOrArrayIndices($record)
{
    expect($record["DonorName"])->toBe("Arnold Schwarzenegger");
    expect($record->name)->toBe("Arnold Schwarzenegger");
}

describe('DonorInfoBuilder', function () {

    beforeEach(function () {
        $this->builder = new DonorInfoBuilder();
    });

    it("takes valid attributes and builds record", function () {
        $record = $this->builder
            ->donorName("Arnold Schwarzenegger")
            ->organization("Big Body Building Actors")
            ->email("arnold@schwarzenegger.com")
            ->build();

        $expected = new DonorInfo([
            "DonorName" => "Arnold Schwarzenegger",
            "Organization/Club" => "Big Body Building Actors",
            "Email" => "arnold@schwarzenegger.com"
        ]);

        expect($record)->toEqual($expected);

    });

    it("donor name required", function() {
        expect(function() {
            $this->builder->donorName("");})->toThrow(new FieldRequiredException);
    });

    it("organization name required", function() {
        expect(function() {
            $this->builder->organization("");})->toThrow(new FieldRequiredException);
    });

    it("email required", function() {
        expect(function() {
            $this->builder->email("");})->toThrow(new FieldRequiredException);
    });

    it("cannot call build unless all fields set",function(){
        expect(function(){
            $this->builder->build();
        })->toThrow("Fields not set");
    });

    it("can access fields as object properties or array indices", function(){
        $record = $this->builder->donorName("Arnold Schwarzenegger")
            ->organization("Big Body Building Actors")
            ->email("arnold@schwarzenegger.com")
            ->build();

        expectAccessFieldsAsObjectsOrArrayIndices($record);
    });




});