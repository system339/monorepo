<?php

use System\Domain\Builders\FoodTypeBuilder;

describe('FoodTypeBuilder', function () {

    it("takes a food type and name", function () {
        $foodType = (new FoodTypeBuilder())->foodType("NonPerishableFood")
            ->name("Pasta")
            ->build();
           
        claim($foodType)->to->equal(["type" => "NonPerishableFood", "name" => "Pasta"]);
    });

    // it("donor name required", function(){
    //     $builder = new FoodTypeBuilder();
    //     claim()->
    //     $builder->name("");

    // })


    // public function donor_name_required()
    // {
        

    //     self::expectException(FieldRequiredException::class);
        
    // }
});
