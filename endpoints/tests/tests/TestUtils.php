<?php

namespace System\Tests;

class TestUtils
{

    /**
     * @param array $areTheyValid
     * @return array|bool
     */
    public static function hasInvalids(array $areTheyValid)
    {
        $hasInvalids = false;
        foreach ($areTheyValid as $they) {
            if ($they === false) {
                $hasInvalids = true;
            }
        }

        return $hasInvalids ? $areTheyValid : true;
    }

    public static function validate(array $entities, $validator)
    {
        $valid = [];
        foreach ($entities as $entity) {
            $valid[] = $validator($entity);
        }
        return TestUtils::hasInvalids($valid);
    }
}
