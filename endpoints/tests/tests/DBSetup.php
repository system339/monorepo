<?php

namespace System\Tests;

use Dotenv\Dotenv;
use System\Domain\Builders\DonorInfoBuilder;
use System\Domain\Builders\FoodTypeBuilder;
use System\Domain\Builders\LineItemBuilder;
use System\Domain\Repositories\Persistance\PersistanceGateway;
use System\Domain\ResourceNames;
use System\Domain\UseCases\DonationManagement\NewDonationEntry;
use System\Domain\UseCases\DonationManagement\ViewExpectedDonation;
use System\Domain\UseCases\OrderManagement\ViewCallAheadOrder;
use System\Persistance\InMemory;
use System\Persistance\JSONFlatFile;
use System\Domain\Builders\DonationEntryBuilder;
use System\Domain\UseCases\DonationManagement\NewExpectedDonation;

trait DBSetup
{
    private PersistanceGateway $persister;

    private ViewExpectedDonation $donationEntryRepo;
    private DonorRepo $donorRepo;
    private ViewCallAheadOrder $orderRepo;

    private NewDonationEntry $newDonationEntry;

    private array $record;
    private $donorID;
    private array $item;
    private array $item2;
    private array $fakeOrder;

    private function startDB()
    {
        $this->persister = $this->loadPersister();

        $this->orderRepo = new ViewCallAheadOrder($this->persister);

        $this->newDonationEntry = new NewDonationEntry($this->persister);
    }

    private function loadFakeData()
    {
        $this->loadDonorInfo();
        $this->loadDonorID();
        $this->loadItems();
        $this->loadDonationEntry();

        return $this->donorID;
    }

    private function loadFakeOrder()
    {
        $this->fakeOrder = [$this->item, $this->item2];
    }

    private function saveFakeDonationEntries($amountOfEntries)
    {
        $donationIDs = [];
        for ($i = 0; $i<$amountOfEntries; $i++) {
            ["id" => $donationID] = $this->newDonationEntry->save($this->record);
            $donationIDs[] = $donationID;
        }

        return count($donationIDs) === 1? $donationIDs[0]:$donationIDs;
    }

    private function loadDonorInfo(): void
    {
        $this->donor = (new DonorInfoBuilder)
            ->donorName("Arnold Schwarzenegger")
            ->organization("Big Body Building Actors")
            ->email("arnold@schwarzenegger.com")
            ->build();
    }

    private function loadDonorID(): void
    {
        $this->donorID =
            $this->persister->save(
                ResourceNames::DONOR_INFO_RESOURCE,
                $this->donor->toArray()
            );
    }

    private function loadItems(): void
    {
        $this->item = (new LineItemBuilder)
            ->quantity(1)
            ->barcode(12345678901234)
            ->name("Body Gear")
            ->expirationDate("01/01/2000")
            ->type((new FoodTypeBuilder)
                ->foodType("NonPerishableFood")
                ->name("Pasta")
                ->build())
            ->build();

        $this->item2 = (new LineItemBuilder)
            ->quantity(1)
            ->barcode(12345678901234)
            ->name("Gear Body")
            ->expirationDate("01/01/2000")
            ->type((new FoodTypeBuilder)
                ->foodType("NonPerishableFood")
                ->name("Pasta")
                ->build())
            ->build();
    }

    private function loadDonationEntry(): void
    {
        $this->record = (new DonationEntryBuilder())
            ->donorId($this->donorID)
            ->items([
                $this->item,
                $this->item2
            ])
            ->build()
            ->toArray();
    }

    /**
     * @return PersistanceGateway
     */
    private function loadPersister()
    {
        $dotenv = Dotenv::createImmutable(__DIR__."/../");
        $dotenv->load();
        $dotenv->required(["DATABASE_TYPE","DATABASE_DIRECTORY"])->notEmpty();


        $databaseType = getenv("DATABASE_TYPE");
        $databaseDirectory = getenv("DATABASE_DIRECTORY");

        if ($databaseType === "memory") {
            return new InMemory;
        } elseif ($databaseType === "filesystem") {
            return new JSONFlatFile($databaseDirectory);
        } else {
            throw new \Exception("database not supported");
        }
    }

    protected function tearDown(): void
    {
        $this->persister->deleteAll();
        $this->persister->close();
    }
}
