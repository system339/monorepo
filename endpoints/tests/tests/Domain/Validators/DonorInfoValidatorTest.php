<?php


namespace System\tests\domain\Validators;

use PHPUnit\Framework\TestCase;
use System\Domain\Validators\DonorInfoValidator;
use System\Tests\TestUtils;

class DonorInfoValidatorTest extends TestCase
{
    private $validator;

    protected function setUp(): void
    {
        $this->validator = new DonorInfoValidator();
    }

    //#region DonorName

    /** @test */
    public function no_arabic_numbers_allowed_in_name()
    {
        $names = [
            "j0hn doe14",
            "john14 doe14",
            "john 123 123 doe"
        ];

        $areTheyValid = TestUtils::validate(
            $names,
            [$this->validator,"validateName"]
        );

        self::assertEquals([false,false,false], $areTheyValid);
    }

    /** @test */
    public function valid_names()
    {
        $names = [
            "John Doe",
            "Juan Ramirez",
            "Jon Doe"
        ];
        $areTheyValid = TestUtils::validate(
            $names,
            [$this->validator,"validateName"]
        );

        self::assertEquals(true, $areTheyValid);
    }

    //#endregion

    //#region Organization/Club


    /** @test */
    public function for_organization_anything_is_valid()
    {
        $organizations = [
            "Women's Soccer Team",
            "Mani-festo 0f r_Evolutionary internS 13",
            "Alph^ @igma B3ta"
        ];

        $valid = TestUtils::validate(
            $organizations,
            [$this->validator,"validateOrganization"]
        );

        self::assertEquals($valid, true);
    }

    //#endregion

    //#region Email

    /** @test */
    public function invalid_email_returns_false()
    {
        $emails = [
            "big @ boi.com", // spaces
            "bigatboi.com" // no @
        ];

        $valid = TestUtils::validate(
            $emails,
            [$this->validator,"validateEmail"]
        );

        self::assertEquals([false,false], $valid);
    }

    /** @test */
    public function valid_email_returns_true()
    {
        $email = "big@boi.com"; // spaces

        $valid = $this->validator->validateEmail($email);

        self::assertEquals(true, $valid);
    }
    //#endregion
}
