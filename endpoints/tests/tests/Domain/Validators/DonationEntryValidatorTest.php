<?php


namespace System\Tests\Domain\Validators;

use PHPUnit\Framework\TestCase;
use System\Domain\Validators\DonationEntryValidator;
use System\Tests\TestUtils;

class DonationEntryValidatorTest extends TestCase
{
    private $validator;

    protected function setUp(): void
    {
        $this->validator = new DonationEntryValidator();
    }



    //#region Timestamp
    /** @test */
    public function timestamp_should_be_valid_unix_timestamp()
    {
        $timestamp = 946688461;

        $valid = $this->validator->validateTimestamp($timestamp);

        self::assertEquals(true, $valid);
    }


    /** @test */
    public function only_integer_allowed_in_timestamp()
    {
        $timestamps = [
            "946688461",
            946688461.01
        ];


        $valid = TestUtils::validate(
            $timestamps,
            [$this->validator,"validateTimestamp"]
        );

        self::assertEquals([false,false], $valid);
    }
    //#endregion
}
