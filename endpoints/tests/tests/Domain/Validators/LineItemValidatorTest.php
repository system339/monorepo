<?php

namespace System\Tests\Domain\Validators;

use PHPUnit\Framework\TestCase;
use System\Domain\Validators\LineItemValidator;
use System\Tests\TestUtils;

class LineItemValidatorTest extends TestCase
{
    private $lineItemValidator;

    protected function setUp(): void
    {
        $this->lineItemValidator = new LineItemValidator;
    }
    //#region quantity

    /** @test */
    public function quantity_must_be_integer()
    {
        $quantities = [1,10,100,1000,"1000",1023.45];

        $actual = TestUtils::validate(
            $quantities,
            [$this->lineItemValidator,"validateQuantity"]
        );

        self::assertEquals([true,true,true,true,false,false], $actual);
    }

    /** @test */
    public function quantity_must_not_be_0()
    {
        $quantities = [0,10,0,1000];

        $actual = TestUtils::validate(
            $quantities,
            [$this->lineItemValidator,"validateQuantity"]
        );

        self::assertEquals([false,true,false,true], $actual);
    }
    //#endregion

    //#region name

    private function validateNames(array $names)
    {
        $actual = [];
        foreach ($names as $name) {
            $actual[] = $this->lineItemValidator->validateName($name);
        }
        return TestUtils::hasInvalids($actual);
    }

    /** @test */
    public function any_name_is_valid()
    {
        $names = ["5pc jello", "big order of tacos", "it_does-not^m{tter@12"];

        $actual = $this->validateNames($names);

        self::assertEquals(true, $actual);
    }
    //#endregion

    //#region barcode

    /** @test */
    public function barcode_must_be_either_12_through_14_digit_integer()
    {
        $barcodes = [
            1234567890123, // 13 digits, good
            12345678901234, // 14 digits, good
            123456789012, // 12 digits, good
            12345678901, // 11 digits, too little
            0, // 1 digit, too little
            12.34, // decimal, not integer
            "12", // string
        ];

        $actual = TestUtils::validate(
            $barcodes,
            [$this->lineItemValidator,"validateBarcode"]
        );

        self::assertEquals(
            [
                true,
                true,
                true,
                false,
                false,
                false,
                false
            ],
            $actual
        );
    }
    //#endregion

    //#region expiration date
    /** @test */
    public function expiration_date_must_be_in_m_d_Y_format()
    {
        $expirations = [
          "01/01/2000", // good
          "01/02/2500", // good
          "0100/02/98", // bad, month has 4 digits
          "1/200/1998" // bad day has 3 digits
        ];

        $actual = TestUtils::validate(
            $expirations,
            [$this->lineItemValidator,"validateExpirationDate"]
        );

        self::assertEquals([
            true,
            true,
            false,
            false
        ], $actual);
    }
    //#endregion

    //#region type


    /**
     * @test
     */
    public function type_must_be_either_NonPerishableFood_or_NonFoodItem()
    {
        $types = [
            ["type"=>"NonPerishableFood","name"=>"Beans"],
            ["type"=>"NonPerishableFood","name"=>"Pasta"],
            ["type"=>"NonPerishableFood","name"=>"Rice"],
            ["type"=>"NonPerishableFood","name"=>"Canned Vegetables"],
            ["type"=>"NonPerishableFood","name"=>"Granola"],
            ["type"=>"NonFoodItem","name"=>"Toothpaste"],
            ["type"=>"NonFoodItem","name"=>"Condoms"],
            ["type"=>"NonFoodItem","name"=>"toilettries"],
            ["type"=>"NonFoodItem","name"=>"detergent"],
            ["type"=>"Item","name"=>"toilettries"],
            ["type"=>"NonFood","name"=>"detergent"],
        ];

        $actual = TestUtils::validate(
            $types,
            [$this->lineItemValidator,"validateType"]
        );

        $expected = [
            true,
            true,
            true,
            true,
            true,
            true,
            true,
            true,
            true,
            false,
            false
        ];

        self::assertEquals($expected, $actual);
    }
    //#endregion
}
