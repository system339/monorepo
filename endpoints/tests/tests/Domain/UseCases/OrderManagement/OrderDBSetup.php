<?php


namespace System\Tests\Domain\UseCases\OrderManagement;

use System\Domain\UseCases\OrderManagement\NewOrder;
use System\Domain\UseCases\OrderManagement\ViewCallAheadOrder;
use System\Domain\UseCases\OrderManagement\ViewOrder;
use System\Tests\DBSetup;

trait OrderDBSetup
{
    use DBSetup;
    private NewOrder $newOrder;
    private ViewCallAheadOrder $orderRepo;
    private ViewOrder $viewOrder;

    public function setUpOrderDB()
    {
        $this->startDB();
        $this->loadFakeData();
        $this->loadFakeOrder();
        $this->orderRepo = new ViewCallAheadOrder($this->persister);
        $this->newOrder = new NewOrder($this->persister);
        $this->viewOrder = new ViewOrder($this->persister);
    }
}
