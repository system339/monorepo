<?php

namespace System\Tests\Domain\UseCases\OrderManagement;

use PHPUnit\Framework\TestCase;
use System\Domain\UseCases\OrderManagement\NewOrder;
use System\Domain\UseCases\OrderManagement\ViewCallAheadOrder;
use System\Tests\Domain\UseCases\ExtraAssertions;

class NewOrderTest extends TestCase
{
    use OrderDBSetup,ExtraAssertions;

    private ViewCallAheadOrder $orderRepo;
    private NewOrder $newOrder;

    protected function setUp(): void
    {
        $this->setUpOrderDB();
    }

    /** @test */
    public function takes_array_of_line_items_returns_receipt_code()
    {
        $receiptCode = $this->newOrder->save($this->fakeOrder);

        self::assertIsString($receiptCode);
        self::assertNotEmpty($receiptCode);
    }

    /** @test */
    public function when_save_called_line_items_are_saved()
    {
        $receiptCode = $this->newOrder->save($this->fakeOrder);

        $savedOrder = $this->viewOrder->findByReceipt($receiptCode);
        self::assertEqualsNoTimestamp($this->fakeOrder, $savedOrder);
    }
}
