<?php

namespace System\Tests\Domain\UseCases\OrderManagement;

use PHPUnit\Framework\TestCase;
use System\Domain\UseCases\OrderManagement\CallAheadOrder;
use System\Domain\UseCases\OrderManagement\NewOrder;
use System\Domain\UseCases\OrderManagement\ViewCallAheadOrder;
use System\Tests\DBSetup;
use System\Tests\Domain\UseCases\ExtraAssertions;

class CallAheadOrderTest extends TestCase
{
    use OrderDBSetup,ExtraAssertions;

    private ViewCallAheadOrder $orderRepo;
    private NewOrder $newOrder;
    private CallAheadOrder $callAheadOrder;

    protected function setUp(): void
    {
        $this->setUpOrderDB();
        $this->callAheadOrder = new CallAheadOrder($this->persister);
    }

    /** @test */
    public function takes_array_of_line_items_returns_receipt_code()
    {
        $order = [$this->item,$this->item2];

        $receiptCode = $this->callAheadOrder->save($order);

        self::assertIsString($receiptCode);
        self::assertNotEmpty($receiptCode);
    }

    /** @test */
    public function when_save_called_line_items_are_saved()
    {
        $order = [$this->item,$this->item2];

        $receiptCode = $this->callAheadOrder->save($order);

        $savedOrder = $this->orderRepo->findCallAheadByReceipt($receiptCode);
        self::assertEqualsNoTimestamp($order, $savedOrder);
    }

    /** @test */
    public function call_ahead_does_not_show_up_on_new_orders()
    {
        $order = [$this->item,$this->item2];
        $receipt1 = $this->newOrder->save($order);
        $receipt2 = $this->newOrder->save($order);

        $this->callAheadOrder->save($order);

        $expectedOrders = [$receipt1 => $order, $receipt2 => $order];
        $actualOrders = $this->viewOrder->findAll();
        self::assertArraysEqualNoTimestamp($expectedOrders, $actualOrders);
    }

    /** @test */
    public function call_ahead_can_be_marked_as_new_order()
    {
        $order = [$this->item,$this->item2];
        $receipt1 = $this->newOrder->save($order);
        $receipt2 = $this->newOrder->save($order);

        $receipt3 = $this->callAheadOrder->save($order);
        $receipt4 = $this->callAheadOrder->markAsNewOrder($receipt3);

        $expectedOrders = [$receipt1 => $order, $receipt2 => $order, $receipt4 => $order];
        $actualOrders = $this->viewOrder->findAll();
        self::assertArraysEqualNoTimestamp($expectedOrders, $actualOrders);
    }
}
