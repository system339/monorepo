<?php

namespace System\Tests\Domain\UseCases\OrderManagement;

use PHPUnit\Framework\TestCase;
use System\Tests\DBSetup;
use System\Tests\Domain\UseCases\ExtraAssertions;
use function date;

class ViewDailyOrdersTest extends TestCase
{
    use OrderDBSetup,ExtraAssertions;

    protected function setUp(): void
    {
        $this->setUpOrderDB();
    }

    /** @test */
    public function given_a_date_returns_list_of_receipt_code()
    {
        $expectedDailyOrders = [
            $this->newOrder->save($this->fakeOrder),
            $this->newOrder->save($this->fakeOrder)
        ];
        $date = date("m-d-Y"); // today's date

        $dailyOrders = $this->dailyOrders($date);

        self::assertEquals($expectedDailyOrders, $dailyOrders);
    }

    /** @test */
    public function older_dates_are_filtered()
    {
        $secondsInADay = 86400;
        $oneDayAgo = \time()-$secondsInADay;
        $savedOrders = [
            $this->newOrder->saveWithTimestamp($this->fakeOrder, $oneDayAgo),
            $this->newOrder->save($this->fakeOrder),
            $this->newOrder->save($this->fakeOrder),
        ];
        $date = date("m-d-Y"); // today's date

        $dailyOrders = $this->dailyOrders($date);

        $expectedDailyOrders = [
            $savedOrders[1],
            $savedOrders[2]
        ];
        self::assertEquals($expectedDailyOrders, $dailyOrders);
    }

    private function dailyOrders($date)
    {
        $all = $this->viewOrder->findAll();
        $filtered = array_filter($all, fn ($record) => $record["Created_at"] === $date);
        return array_keys($filtered);
    }
}
