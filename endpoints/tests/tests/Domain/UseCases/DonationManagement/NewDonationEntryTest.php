<?php


namespace System\Tests\Domain\UseCases\DonationManagement;

use PHPUnit\Framework\TestCase;
use System\Domain\UseCases\DonationManagement\Requests\DonationEntry;
use System\Domain\UseCases\DonationManagement\ViewDonationEntry;
use System\Tests\DBSetup;

class NewDonationEntryTest extends TestCase
{
    use DBSetup;

    private DonationEntry $entry;

    private function loadDonationEntryFakeData()
    {
        $donorID = $this->loadFakeData();

        $nonPerishableType = [
            "type"=>"NonPerishableFood",
            "name"=>"Pasta"
        ];
        $nonFoodType = [
            "type"=>"NonFoodItem",
            "name"=>"Gear"
        ];
        $items = [
            [
                "quantity"=>1,
                "barcode" => 12345678901234,
                "name" => "Body Gear",
                "expiration_date"=>"01/01/2000",
                "type"=> $nonPerishableType
            ],
            [
                "quantity"=>2,
                "barcode" => 12346678901234,
                "name" => "Bigger Body Gear",
                "expiration_date"=>"01/01/2000",
                "type"=> $nonFoodType
            ],
        ];

        $this->entry = new DonationEntry([
            "DonorID" => $donorID,
            "Items"=> $items
        ]);
    }

    private ViewDonationEntry $viewDonationEntry;

    protected function setUp(): void
    {
        $this->startDB();

        $this->loadDonationEntryFakeData();
        $this->viewDonationEntry = new ViewDonationEntry($this->persister);
    }

    /** @test */
    public function validates_invalid_keys()
    {
        $entry = [
            "D0norId" => $this->entry["DonorID"], // should be DonorID
            "Item"=> $this->entry["Items"], // should be Items
            "ExtraKey"=>"hehe"
        ];
        
        self::expectExceptionMessage("Invalid Keys");
        $this->newDonationEntry->save($entry);
    }

    /** @test */
    public function throws_if_donor_id_does_not_exist()
    {
        $this->entry["DonorID"] = 12345; // doesnt exist

        self::expectExceptionMessage("Donor ID not found");
        $this->newDonationEntry->save($this->entry);
    }
    
    /** @test */
    public function valid_donation_entry_is_saved_with_creation_date()
    {
        $unix = time();
        ["id" => $id, "Created_at" => $createdAt] = $this->newDonationEntry->save($this->entry);

        $donationEntry = $this->viewDonationEntry->view($id);

        self::assertEquals($donationEntry["Created_at"], $createdAt);
        self::assertGreaterThanOrEqual($unix, $createdAt);
    }

    /** @test */
    public function takes_a_valid_donation_entry_and_returns_an_id()
    {
        ["id" => $id] = $this->newDonationEntry->save($this->entry);

        $whatWasSaved = $this->viewDonationEntry->view($id);
        unset($whatWasSaved["Created_at"]);
        self::assertEquals($this->entry, new DonationEntry($whatWasSaved));
    }

    /** @test */
    public function delegates_rest_of_validation()
    {
        $entry = $this->entryWithModifiedItemQuantity("3"); // cant be a string

        self::expectExceptionMessage("Invalid LineItem at index 0");
        $this->newDonationEntry->save($entry);
    }

    protected function tearDown(): void
    {
        $this->persister->deleteAll();
        $this->persister->close();
    }

    private function entryWithModifiedItemQuantity($quantity)
    {
        $items = $this->entry["Items"];
        $item = $items[0];
        $item["quantity"] = $quantity;
        $items[0] = $item;
        $this->entry["Items"] = $items;
        return $this->entry;
    }
}
