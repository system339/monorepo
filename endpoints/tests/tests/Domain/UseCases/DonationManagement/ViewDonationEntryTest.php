<?php

namespace System\Tests\Domain\UseCases\DonationManagement;

use PHPUnit\Framework\TestCase;
use System\Domain\UseCases\DonationManagement\ViewDonationEntry;
use System\Tests\DBSetup;
use System\Tests\Domain\UseCases\ExtraAssertions;

class ViewDonationEntryTest extends TestCase
{
    use DBSetup, ExtraAssertions;
    private ViewDonationEntry $viewDonationEntry;

    protected function setUp(): void
    {
        $this->startDB();
        $this->loadFakeData();
        $this->viewDonationEntry = new ViewDonationEntry($this->persister);
    }

    /** @test */
    public function takes_a_donation_id_and_returns_a_donation()
    {
        $donationID = self::saveFakeDonationEntries(1);

        $retrieved = $this->viewDonationEntry->view($donationID);
        
        self::assertEqualsNoTimestamp($this->record, $retrieved);
    }

    /** @test */
    public function takes_a_donor_id_and_returns_all_donations()
    {
        $donationIDs = self::saveFakeDonationEntries(3);
        
        $view = $this->viewDonationEntry->viewAll($this->record["DonorID"]);
        
        $viewKeys = array_keys($view);
        for ($i = 0; $i < 3; $i++) {
            self::assertEquals($viewKeys[$i], $donationIDs[$i]); // keys are donation ids
            self::assertEqualsNoTimestamp($this->record, $view[$donationIDs[$i]]); //values are $records
        }
    }

    protected function tearDown(): void
    {
        $this->persister->deleteAll();
        $this->persister->close();
    }
}
