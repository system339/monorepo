<?php

namespace System\Tests\Domain\UseCases\DonationManagement\Builders;

use PHPUnit\Framework\TestCase;
use System\Domain\Builders\DonorInfoBuilder;
use System\Domain\Builders\FieldRequiredException;
use System\Domain\UseCases\DonationManagement\Requests\DonorInfo;

class DonorInfoBuilderTest extends TestCase
{
    /** @test */
    public function it_takes_valid_attributes_and_builds_record()
    {
        $builder = new DonorInfoBuilder;

        $record = $builder->donorName("Arnold Schwarzenegger")
            ->organization("Big Body Building Actors")
            ->email("arnold@schwarzenegger.com")
            ->build();

        $expected = new DonorInfo([
            "DonorName" => "Arnold Schwarzenegger",
            "Organization/Club" => "Big Body Building Actors",
            "Email" => "arnold@schwarzenegger.com"
        ]);
        self::assertEquals($expected, $record);
    }

    /** @test */
    public function donor_name_required()
    {
        $builder = new DonorInfoBuilder;

        self::expectException(FieldRequiredException::class);
        $builder->donorName("");
    }

    /** @test */
    public function organization_required()
    {
        $builder = new DonorInfoBuilder;

        self::expectException(FieldRequiredException::class);
        $builder->organization("");
    }

    /** @test */
    public function email_required()
    {
        $builder = new DonorInfoBuilder;

        self::expectException(FieldRequiredException::class);
        $builder->email("");
    }

    /** @test */
    public function cannot_call_build_unless_all_fields_set()
    {
        $builder = new DonorInfoBuilder;

        self::expectExceptionMessage("Fields not set");
        $builder->build();
    }

    /** @test */
    public function can_access_fields_as_object_properties_or_array_indices()
    {
        $builder = new DonorInfoBuilder();
        $record = $builder->donorName("Arnold Schwarzenegger")
            ->organization("Big Body Building Actors")
            ->email("arnold@schwarzenegger.com")
            ->build();

        self::assertEquals($record["DonorName"], "Arnold Schwarzenegger");
        self::assertEquals($record->name, "Arnold Schwarzenegger");
    }
}
