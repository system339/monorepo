<?php

namespace System\Tests\Domain\UseCases\DonationManagement\Builders;

use PHPUnit\Framework\TestCase;
use System\Domain\Builders\FieldRequiredException;
use System\Domain\Builders\FoodTypeBuilder;

class FoodTypeBuilderTest extends TestCase
{
    /** @test */
    public function takes_a_food_type_and_name()
    {
        $foodType = (new FoodTypeBuilder())->foodType("NonPerishableFood")
            ->name("Pasta")
            ->build();
        
            

        self::assertEquals(["type" => "NonPerishableFood", "name" => "Pasta"], $foodType);
    }

    

    /** @test */
    public function type_must_be_either_NonPerishableFood_or_NonFoodItem()
    {
        $foodType = (new FoodTypeBuilder())->foodType("NonPerishableFood")
            ->name("Pasta")
            ->build();
        self::assertEquals(["type" => "NonPerishableFood", "name" => "Pasta"], $foodType);

        $foodType = (new FoodTypeBuilder())->foodType("NonFoodItem")
            ->name("Pasta")
            ->build();
        self::assertEquals(["type" => "NonFoodItem", "name" => "Pasta"], $foodType);

        self::expectExceptionMessage("Invalid food type: must be either 'NonPerishableFood' or 'NonFood'");
        (new FoodTypeBuilder())->foodType("SomeOtherType");
    }

    /** @test */
    public function donor_name_required()
    {
        $builder = new FoodTypeBuilder();

        self::expectException(FieldRequiredException::class);
        $builder->name("");
    }
}
