<?php

namespace System\Tests\Domain\UseCases\DonationManagement\Builders;

use PHPUnit\Framework\TestCase;
use System\Domain\Builders\DonationEntryBuilder;
use System\Domain\UseCases\DonationManagement\Requests\DonationEntry;
use System\Tests\DBSetup;

class DonationEntryBuilderTest extends TestCase
{
    use DBSetup;

    protected function setUp(): void
    {
        $this->startDB();
        $this->loadFakeData();
    }


    /** @test */
    public function takes_donor_id_and_items_returns_record()
    {
        $items = [$this->item, $this->item2];
        $donorID = $this->donorID;

        $record = (new DonationEntryBuilder)
            ->donorId($donorID)
            ->items($items)
            ->build();
        
     
        self::assertEquals(
            new DonationEntry([
                "DonorID" => $donorID,
                "Items" => $items
            ]),
            $record
        );
    }

    /** @test */
    public function two_donation_entries_are_equal_if_args_are_equal()
    {
        $items1 = [$this->item, $this->item2];
        $items2 = [$this->item2, $this->item];
        $donorID = $this->donorID;

        $record1 = (new DonationEntryBuilder)
            ->donorId($donorID)
            ->items($items1)
            ->build();
        $record2 = (new DonationEntryBuilder)
            ->donorId($donorID)
            ->items($items2)
            ->build();

        self::assertNotEquals($record1, $record2);
    }

    /** @test */
    public function can_convert_donationEntry_to_array()
    {
        $items1 = [$this->item, $this->item2];
        $donorID = $this->donorID;

        $record1 = (new DonationEntryBuilder)
            ->donorId($donorID)
            ->items($items1)
            ->build();

        self::assertEquals([
            "DonorID" => $donorID,
            "Items" => $items1
        ], $record1->toArray());
    }
}
