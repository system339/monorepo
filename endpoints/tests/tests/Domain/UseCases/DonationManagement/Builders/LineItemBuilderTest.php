<?php

namespace System\Tests\Domain\UseCases\DonationManagement\Builders;

use PHPUnit\Framework\TestCase;
use System\Domain\Builders\FoodTypeBuilder;
use System\Domain\Builders\LineItemBuilder;

class LineItemBuilderTest extends TestCase
{
    /** @test */
    public function takes_quantity_barcode_name_expirationDate_foodType()
    {
        $quantity = 1;
        $barcode = 12345678901234;
        $name = "Body Gear";
        $expirationDate = "01/01/2000";
        $foodType = (new FoodTypeBuilder())->foodType("NonPerishableFood")->name("Pasta")->build();

        $lineItem = (new LineItemBuilder())->quantity($quantity)->
            barcode($barcode)->
            name($name)->
            expirationDate($expirationDate)->
            type($foodType)->
            build();
        
        self::assertEquals(
            [
                "quantity"=>$quantity,
                "barcode"=>$barcode,
                "name"=>$name,
                "expiration_date"=>$expirationDate,
                "type"=>$foodType
            ],
            $lineItem
        );
    }
}
