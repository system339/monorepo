<?php

namespace System\Tests\Domain\UseCases\DonationManagement\Requests;

use PHPUnit\Framework\TestCase;
use System\Domain\UseCases\DonationManagement\Requests\DonorInfo;
use System\Domain\Validators\InvalidDonorException;

class DonorInfoTest extends TestCase
{
    /** @test */
    public function validates_wrong_keys()
    {
        self::expectExceptionMessage("Invalid Keys");
        new DonorInfo([
            "Name" => "Arnold Schwarzenegger", // should be "DonorName"
            "Organization/Club" => "Big Body Building Actors",
            "Emaill" => "arnold@schwarzenegger.com" // should be "Email"
        ]);
    }

    /** @test */
    public function delegates_rest_of_validation()
    {
        try {
            new DonorInfo([
                "DonorName" => "Arn0ld Schw2rz3n3gg3r", // no numbers
                "Organization/Club" => "Big Body Building Actors",
                "Email" => "arnold_schwarzenegger.com" // no @
            ]);
        } catch (InvalidDonorException $e) {
            self::assertEquals(["DonorName","Email"], $e->invalidAttrs());
            return;
        }
        self::fail("This constructor should throw");
    }
}
