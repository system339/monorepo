<?php

namespace System\Tests\Domain\UseCases\DonationManagement;

use PHPUnit\Framework\TestCase;
use System\Domain\UseCases\DonationManagement\NewDonationEntry;
use System\Domain\UseCases\DonationManagement\UndoDonationEntry;
use System\Domain\UseCases\DonationManagement\UpdateDonationEntry;
use System\Domain\UseCases\DonationManagement\ViewDonationEntry;
use System\Tests\DBSetup;

class UpdateDonationEntryTest extends TestCase
{
    use DBSetup;

    /**
     * @var UpdateDonationEntry
     */
    private UpdateDonationEntry $updateDonationEntry;
    /**
     * @var ViewDonationEntry
     */
    private ViewDonationEntry $viewDonationEntry;


    protected function setUp() : void
    {
        $this->startDB();
        $this->loadFakeData();
        $this->updateDonationEntry= new UpdateDonationEntry($this->persister);
        $this->viewDonationEntry = new ViewDonationEntry($this->persister);
    }
    /** @test */
    public function when_no_items_then_no_changes()
    {
        $donationId = $this->saveFakeDonationEntries(1);
        $before = $this->viewDonationEntry->view($donationId);

        $updatedDonationId = $this->updateDonationEntry->update($donationId, []);
        
        $after = $this->viewDonationEntry->view($updatedDonationId);
        self::assertEquals($before, $after);
    }

    /** @test */
    public function it_undos_the_previous_entry_and_creates_a_new_entry_with_fields()
    {
        $donationId = $this->saveFakeDonationEntries(1);

        $newName = "The biggest Mac & Cheese ever";
        $updatedDonationId = $this->updateDonationEntry->update($donationId, [
            [
                "name" => $newName
            ]
        ])["id"];

        $after = $this->viewDonationEntry->viewAll($this->donorID);
        self::assertCount(3, $after);
        $this->assertFirstItemNameEquals($after, $updatedDonationId, $newName);
    }

    /**
     * @param $after
     * @param $updatedDonationId
     * @param $newName
     */
    private function assertFirstItemNameEquals($after, $updatedDonationId, $newName): void
    {
        self::assertEquals(
            $newName,
            $after[$updatedDonationId]["Items"][0]["name"]
        );
    }

    //TODO: test that the same donation can only be updated once
}
