<?php


namespace System\Tests\Domain\UseCases\DonationManagement;

use PHPUnit\Framework\TestCase;
use System\Domain\Repositories\Persistance\PersistanceGateway;
use System\Domain\UseCases\DonationManagement\NewDonor;
use System\Domain\UseCases\DonationManagement\Requests\DonorInfo;
use System\Domain\Validators\InvalidDonorException;
use System\Tests\DBSetup;

class NewDonorTest extends TestCase
{
    use DBSetup;

    private NewDonor $newDonor;

    /** @before */
    protected function startUpDonors(): void
    {
        $this->startDB();
    }

    /** @test */
    public function takes_valid_donor_info_returns_id()
    {
        $spyPersister = new class {
            public $whatWasSaved;
            public function save(string $resource, array $data)
            {
                $this->whatWasSaved = ["resource" => $resource,"data" => $data];
            }
        };

        $spyPresenter = new class {
        };
        $this->newDonor = new NewDonor($spyPersister, $spyPresenter);
        $donor = new DonorInfo([
            "DonorName" => "Arnold Schwarzenegger",
            "Organization/Club" => "Big Body Building Actors",
            "Email" => "arnold@schwarzenegger.com"
        ]);

        $this->newDonor->save($donor);

        self::assertEquals(
            $donor->toArray(),
            $spyPersister->whatWasSaved["data"]
        );
    }
}
