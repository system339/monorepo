<?php

namespace System\Tests\Domain\UseCases\DonationManagement;

use PHPUnit\Framework\TestCase;
use System\Domain\UseCases\DonationManagement\NewExpectedDonation;
use System\Domain\UseCases\DonationManagement\ViewDonationEntry;
use System\Domain\UseCases\DonationManagement\ViewExpectedDonation;
use System\Tests\DBSetup;
use System\Tests\Domain\UseCases\ExtraAssertions;

class NewExpectedDonationTest extends TestCase
{
    use DBSetup, ExtraAssertions;
    private NewExpectedDonation $newExpectedDonation;

    private ViewDonationEntry $viewDonationEntry;
    /**
     * @var ViewExpectedDonation
     */
    private ViewExpectedDonation $viewExpectedDonation;

    protected function setUp(): void
    {
        $this->startDB();
        $this->loadFakeData();
        $this->newExpectedDonation = new NewExpectedDonation($this->persister);
        $this->viewDonationEntry = new ViewDonationEntry($this->persister);

        $this->viewExpectedDonation = new ViewExpectedDonation($this->persister);
    }

    /** @test */
    public function takes_valid_record_then_record_is_saved()
    {
        $record = $this->record;

        $expectedId = $this->newExpectedDonation->save($record)["id"];

        $actualRecord = ($this->viewExpectedDonation->viewExpectedById($expectedId));
        self::assertEqualsNoTimestamp($record, $actualRecord);
    }

    /** @test */
    public function expected_donation_can_be_marked_as_new_donation()
    {
        $record = $this->record;
        $expectedId = $this->newExpectedDonation->save($record)["id"];

        $donationId = $this->newExpectedDonation->markAsNewDonation($expectedId)["id"];

        $actualRecord = $this->viewDonationEntry->view($donationId);
        self::assertEqualsNoTimestamp($record, $actualRecord);
    }
}
