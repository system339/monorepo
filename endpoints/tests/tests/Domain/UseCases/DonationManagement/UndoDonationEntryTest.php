<?php

namespace System\Tests\Domain\UseCases\DonationManagement;

use PHPUnit\Framework\TestCase;
use System\Domain\UseCases\DonationManagement\UndoDonationEntry;
use System\Domain\UseCases\DonationManagement\ViewDonationEntry;
use System\Tests\DBSetup;

class UndoDonationEntryTest extends TestCase
{
    use DBSetup;

    private ViewDonationEntry $viewDonationEntry;

    protected function setUp() : void
    {
        $this->startDB();
        $this->loadFakeData();
        $this->undoDonationEntry = new UndoDonationEntry($this->persister);
        $this->viewDonationEntry = new ViewDonationEntry($this->persister);
    }

    /** @test */
    public function takes_a_donation_entry_id_returns_id_of_undo_entry()
    {
        $entryId = $this->saveFakeDonationEntries(1);
        
        $undoEntryId = $this->undoDonationEntry->undo($entryId);
        
        $undoEntry = $this->viewDonationEntry->view($undoEntryId);
        self::assertIsString($undoEntryId);
    }

    /** @test */
    public function throws_if_entry_id_not_found()
    {
        $entryId = "123456456123";
        
        self::expectExceptionMessage("Donor ID not found");
        $undoEntryId = $this->undoDonationEntry->undo($entryId);
    }

    private function assertIsNegativeCounterpart($entryId, $undoEntryId)
    {
        $entryItems = array_values($this->viewDonationEntry->view($entryId)["Items"]);
        $undoItems = array_values($this->viewDonationEntry->view($undoEntryId)["Items"]);
        for ($i=0; $i<count($undoItems); $i++) {
            self::assertEquals(-($entryItems[$i]["quantity"]), $undoItems[$i]["quantity"]);
        }
    }

    /** @test */
    public function quantities_in_undo_entry_are_negative_counterpart()
    {
        $entryId = $this->saveFakeDonationEntries(1);

        $undoEntryId = $this->undoDonationEntry->undo($entryId);
        
        self::assertIsNegativeCounterpart($entryId, $undoEntryId);
    }

    protected function tearDown() : void
    {
        $this->persister->deleteAll();
        $this->persister->close();
    }
}
