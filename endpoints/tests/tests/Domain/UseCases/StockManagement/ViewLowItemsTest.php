<?php

namespace System\Tests\Domain\UseCases\StockManagement;

use PHPUnit\Framework\TestCase;
use System\Domain\UseCases\OrderManagement\NewOrder;
use System\Domain\UseCases\StockManagement\ViewLowItems;
use System\Tests\DBSetup;
use System\Tests\Domain\UseCases\ExtraAssertions;

class ViewLowItemsTest extends TestCase
{
    use DBSetup,ExtraAssertions;
    private ViewLowItems $viewLowItems;
    private NewOrder $newOrder;

    protected function setUp(): void
    {
        $this->startDB();
        $this->loadFakeData();
        $this->loadFakeOrder();
        $this->viewLowItems = new ViewLowItems($this->persister);
        $this->newOrder = new NewOrder($this->persister);
    }

    /** @test */
    public function view_returns_all_items_with_quantities_less_than_4()
    {
        $this->loadDifferentItemsWithQuantities([3,4]);

        $this->newDonationEntry->save($this->record);
        $items = $this->viewLowItems->view();

        self::assertEquals([
            $this->item["name"]=>3 // less than 4
        ], $items);
    }

    /** @test */
    public function same_name_item_have_quantities_added()
    {
        $this->loadSameNameEntriesWithQuantities([3,4]); // 3+4=7, so no

        $this->newDonationEntry->save($this->record);
        $items = $this->viewLowItems->view();

        self::assertEquals([], $items);
    }

    /** @test */
    public function orders_subtract_quantity_from_items()
    {
        $this->loadSameNameEntriesWithQuantities([3,4]);
        $this->loadSameNameOrdersWithQuantities([4]); // 3+4-4 = 3, so items shows up


        $this->newDonationEntry->save($this->record);
        $items = $this->viewLowItems->view();

        self::assertEquals([
            $this->item["name"]=>3
        ], $items);
    }

    private function loadSameNameEntriesWithQuantities($quantities): void
    {
        $this->item["quantity"] = $quantities[0];
        $this->item2 = $this->item;
        $this->item2["quantity"] = $quantities[1];
        $this->loadDonationEntry();
    }

    private function loadDifferentItemsWithQuantities($quantities): void
    {
        $this->item["quantity"] = $quantities[0];
        $this->item2["quantity"] = $quantities[1];
        $this->loadDonationEntry();
    }

    private function loadSameNameOrdersWithQuantities(array $quantities)
    {
        $this->item["quantity"] = $quantities[0];
        $this->fakeOrder=[
            $this->item,
        ];
        $this->newOrder->save($this->fakeOrder);
    }
}
