<?php


namespace System\Tests\Domain\UseCases;

trait ExtraAssertions
{
    public function assertEqualsNoTimestamp($record, $retrieved)
    {
        unset($retrieved["Created_at"]);
        self::assertEquals($record, $retrieved);
    }

    public function assertArraysEqualNoTimestamp($recordList, $retrievedList)
    {
        foreach ($retrievedList as $key => $retrieved) {
            unset($retrieved["Created_at"]);
            $retrievedList[$key] = $retrieved;
        }
        self::assertEquals($recordList, $retrievedList);
    }
}
