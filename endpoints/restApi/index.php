<?php

require './vendor/autoload.php';

use Slim\Factory\AppFactory;
use Slim\Exception\HttpNotFoundException;
use Psr\Http\Server\RequestHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use System\Rest\Controllers\HomeController;
use System\Rest\UseCaseFactory;
use System\Rest\UseCases\OutputPort;


$builder = new ContainerBuilder();

$container = $builder;

AppFactory::setContainer($container);
$app = AppFactory::create();
$app->addRoutingMiddleware();
$app->addErrorMiddleware(true, true, true);


$container->set(UseCaseFactory::class, new UseCaseFactory($container));
$container->register(HomeController::class, HomeController::class);

$app->add(function ($request, RequestHandlerInterface $handler) use ($container) {
    $resp = $handler->handle($request);

    var_dump($container);
    var_dump($resp);
    $presenter = $container->get(OutputPort::class);
    $resp->getBody()->write($presenter->response);
    var_dump($resp);
    return $resp;
});


//$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
//    $controller = $this->get("Controller");
//    $presenter = $this->get("Presenter");
//    $name = $args['name'];
//
//    $controller->get($name);
//    $html = $presenter->presentHTML();
//
//    $response->getBody()->write($html);
//    return $response;
//});

$app->get('/', HomeController::class.":home");


$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});




/**
 * Catch-all route to serve a 404 Not Found page if none of the routes match
 * NOTE: make sure this route is defined last
 */
$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function ($request, $response) {
    throw new HttpNotFoundException($request);
});



$app->add(function ($request, $handler) {
    $response = $handler->handle($request);
    return $response
        ->withHeader('Access-Control-Allow-Origin', 'http://localhost:3000')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$app->run();
echo "Hi";
