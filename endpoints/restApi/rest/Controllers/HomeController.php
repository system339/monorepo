<?php

namespace System\Rest\Controllers;

use System\Rest\UseCaseFactory;

class HomeController {
    private $case;

    public function __construct(UseCaseFactory $factory)
    {
        $this->case = $factory->makeUseCase();
    }

    public function home($req, $resp, $args) {
        $this->case->execute();

        return $resp;
    }
}