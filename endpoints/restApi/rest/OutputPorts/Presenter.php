<?php

namespace System\Rest\OutputPorts;

use System\Rest\UseCases\OutputPort;

class Presenter implements OutputPort
{
    public string $response;

    public function respond(string $response)
    {
        $this->response = ($response);
    }
}