<?php


namespace System\Rest;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use System\Rest\OutputPorts\Presenter;
use System\Rest\UseCases\OutputPort;
use System\Rest\UseCases\UseCase;

class UseCaseFactory
{
    private OutputPort $outputPort;

    private $container;

    public function __construct($container)
    {
        $this->container=$container;
        $this->outputPort = new Presenter();
        $this->container->set(UseCase::class, new UseCase($this->outputPort));
    }

    public function makeUseCase()
    {
        return $this->container->get(UseCase::class);
    }
}