<?php

namespace System\Rest\UseCases;

class UseCase {

    private OutputPort $out;

    public function __construct(OutputPort $out)
    {
        $this->out = $out;
    }

    public function execute()
    {
        $this->out->respond("Hello guys");
    }
}