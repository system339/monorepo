<?php

namespace System\Rest\UseCases;

interface OutputPort {
    public function respond(string $response);
}