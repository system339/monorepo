#!/usr/bin/env php
<?php

function out($s){
    echo "$s\n";
}

function build() {
    $build = `pnpm run build:ci`;
    out($build);
    $cmd = `echo "$build" | grep -Po "Cannot resolve dependency '(\K.+)(?=')"`;
    return $cmd;
}

$cmd = build();
while($cmd !== "") {
    $dep = $cmd;
    $dep = explode("/",$dep)[0];

    out("Installing missing dep $dep");

    $cmd = `pnpm install $dep`;

    out($cmd);

    $cmd = build();
}
