# Development tools in the react project
- EditorConfig: enforces tabs and spacing accross ides
- Browsersync: live reloads and lets you share your server accross devices in your network 
- Ngrok: quickly share your current server with people during development (e.g Browsersync)
- NPM Scripts: this is our main you will see many in this projects package.json
- Babel: works with React and for extra syntax plugins
- Parcel: bundler for our application and recompiles on watch
- ESLint: lints project for errors in javascript
- Jest: Testing frameworks, also can do dom testing
- Husky: run git hooks before commit or pushing
