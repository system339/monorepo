import React from "react"
import ReactDOM from "react-dom"
import Home from "./components/Views/Home/Home"
import Register from "./components/Views/Register/Register"
import Login from "./components/Views/Login/Login"
import "./scss/app.scss"
import Inventory from "./components/Views/Inventory/Inventory"
import DonationEntry from "./components/Views/DonationEntry/DonationEntry"
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"

let App = document.getElementById("app")

ReactDOM.render(
  <Router>
    <Switch>
      <Route exact path="/">
        <Login />
      </Route>
      <Route path="/home">
        <Home />
      </Route>
      <Route path="/register">
        <Register />
      </Route>
      <Route path="/login">
        <Login />
      </Route>
      <Route path="/inventory">
        <Inventory />
      </Route>
      <Route path="/login">
        <Login />
      </Route>
      <Route path="/reports">
        <DonationEntry />
      </Route>
    </Switch>
  </Router>,
  App
)

//Make form responsive
//Make pass/fail error component
