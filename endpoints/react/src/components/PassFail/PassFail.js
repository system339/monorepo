import React from "react"

const PassFail = ({ passText, failText, passfail }) =>
  passfail ? (
    <p style={{ color: "green" }}>{passText}</p>
  ) : (
    <p style={{ color: "#990000" }}>{failText}</p>
  )

export default PassFail
