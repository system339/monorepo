import React from "react"
import Layout from "../Layout/Layout"
import Header from "../Header/Header"
import SideNav from "../SideBar/SideBar"
import styles from "./DashBoardLayout.module.scss"

const DashBoardLayout = ({ children }) => {
  return (
    <Layout>
      <Header />
      <div className={styles.container}>
        <SideNav />
        <div className={styles.content}>
          <p></p>
          {children}
        </div>
      </div>
    </Layout>
  )
}

export default DashBoardLayout
