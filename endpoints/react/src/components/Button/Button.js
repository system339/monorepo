import React from "react"
import styles from "./Button.module.scss"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink,
} from "react-router-dom"
const Button = ({ text, color, children, textColor, padding, click, path }) => {
  const styleObj = {
    backgroundColor: color,
    color: textColor,
  }
  return (
    <Link to={path}>
      <button
        onClick={click}
        type="submit"
        className={styles.button}
        style={{ ...styleObj, paddingTop: padding }}
        href="/"
      >
        {text}
      </button>
    </Link>
  )
}

export default Button
