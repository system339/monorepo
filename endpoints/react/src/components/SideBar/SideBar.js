import React, { Fragment } from "react"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink,
} from "react-router-dom"
import SideNav, {
  Toggle,
  Nav,
  NavItem,
  NavIcon,
  NavText,
} from "@trendmicro/react-sidenav"
import "@trendmicro/react-sidenav/dist/react-sidenav.css"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
  faHome,
  faCube,
  faPaste,
  faUserCheck,
  faCheck,
  faCog,
  faSignOutAlt,
} from "@fortawesome/free-solid-svg-icons"

import Home from "../Views/Home/Home"

import Inventory from "../Views/Inventory/Inventory"
const SideBar = () => {
  return (
    <Route
      render={({ location, history }) => (
        <React.Fragment>
          <SideNav
            onSelect={selected => {
              const to = "/" + selected
              if (location.pathname !== to) {
                history.push(to)
              }
            }}
          >
            <SideNav.Toggle />
            <SideNav.Nav>
              <NavItem eventKey="home">
                <NavIcon>
                  <FontAwesomeIcon icon={faHome} />
                </NavIcon>
                <NavText>Home</NavText>
              </NavItem>
              <NavItem eventKey="inventory">
                <NavIcon>
                  <FontAwesomeIcon icon={faCube} />
                </NavIcon>
                <NavText>Inventory</NavText>
              </NavItem>
              <NavItem eventKey="reports">
                <NavIcon>
                  <FontAwesomeIcon icon={faCheck} />
                </NavIcon>
                <NavText>Reports</NavText>
              </NavItem>
              <NavItem eventKey="settings">
                <NavIcon>
                  <FontAwesomeIcon icon={faCog} />
                </NavIcon>
                <NavText>Settings</NavText>
              </NavItem>
              <NavItem eventKey="login">
                <NavIcon>
                  <FontAwesomeIcon icon={faSignOutAlt} />
                </NavIcon>
                <NavText>Sign Out</NavText>
              </NavItem>
            </SideNav.Nav>
          </SideNav>
        </React.Fragment>
      )}
    />
  )
}

export default SideBar
