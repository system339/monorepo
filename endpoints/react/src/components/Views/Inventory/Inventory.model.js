const InventoryModel = () => {
  let callBacks = []
  return {
    updateInventory: function () {
      fetch(`https://jsonplaceholder.typicode.com/users`)
        .then(response => response.json())
        .then(info => callBacks.forEach(c => c(info)))
    },
    subscribe: function (setInventoryData) {
      callBacks.push(setInventoryData)
    },
  }
}

export default InventoryModel
