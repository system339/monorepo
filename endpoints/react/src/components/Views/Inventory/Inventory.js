import React, { Fragment, useState, useEffect } from "react"
import DashBoardLayout from "../../DashboardLayout/DashBoardLayout"
import styles from "./Inventory.module.scss"
import Button from "../../Button/Button"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faSearch, faPlusCircle } from "@fortawesome/free-solid-svg-icons"
import Swiper from "react-id-swiper"
import logo from "@/images/seawolves.png"
import InventoryModel from "./Inventory.model"
import { withRouter } from "react-router"

const SearchBar = () => {
  return (
    <div className={styles.wrap}>
      <div className={styles.search}>
        <input
          type="text"
          className={styles.searchTerm}
          placeholder="Search?"
        />
        <button type="submit" className={styles.searchButton}>
          <FontAwesomeIcon icon={faSearch} />
        </button>
      </div>
    </div>
  )
}

const InventoryController = model => () => {
  model.updateInventory()
}

const InventoryView = () => {
  const [state, setstate] = useState([])
  let model = InventoryModel()
  model.subscribe(setstate)
  let controller = InventoryController(model)

  const params = {
    slidesPerView: 3,
    spaceBetween: 30,
    freeMode: true,
    freeModeMomentumVelocityRatio: 0.5,
    wrapperClass: styles.swiperWrapper,
    slideClass: styles.inventoryItem,
    grabCursor: true,
    direction: "vertical",
    observer: true,
    speed: 20,

    scrollbar: {
      el: ".swiper-scrollbar",
      hide: false,
    },
    mousewheel: true,
  }

  const inventoryItems = (
    <Swiper {...params}>
      {state.map(item => {
        return (
          <div key={item.id}>
            <p>JQ345</p>
            <img src={logo} />
            <p>{item.name}</p>
            <p>{item.username}</p>
            <p>25/72</p>
          </div>
        )
      })}
    </Swiper>
  )

  useEffect(() => {
    controller()
  })
  return <Fragment>{inventoryItems}</Fragment>
}

const Inventory = () => {
  return (
    <DashBoardLayout>
      <div className={styles.container}>
        <SearchBar />
        <select name="searchtype">
          <option value="all">All</option>
          <option value="Canned Food">Brooklyn</option>
          <option value="nonedible">Nonedible</option>
        </select>
        <div className={styles.buttonContainer}>
          <Button
            text="New Order"
            textColor="white"
            color="#990000"
            symbol={faSearch}
          >
            <FontAwesomeIcon icon={faPlusCircle} />
          </Button>
        </div>
      </div>
      <div className={styles.itemContainer}>
        <div className={styles.titles}>
          <p>Product Code</p>
          <p>Product</p>
          <p></p>
          <p>Date</p>
          <p>Quantity Left</p>
        </div>
        <InventoryView />
      </div>
    </DashBoardLayout>
  )
}

export default withRouter(Inventory)
