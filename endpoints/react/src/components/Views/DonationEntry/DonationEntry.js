import React, { Fragment, useState, useEffect } from "react"

import DashBoardLayout from "../../DashboardLayout/DashBoardLayout"
import Input from "../../Input/Input"
import styles from "../DonationEntry/DonationEntry.module.scss"
import Button from "../../Button/Button"

const FormItem = ({ change, numChange }) => {
  // const [state, setState] = setState([]);
  // const onSubmit = (e) => {
  //   const value = e.target.value;
  //   setState(state.concat(value))
  // };
  return (
    <Fragment>
      <div className={styles.formItem}>
        <Input type="text" name="Item Name" onChange={change} />
        <Input
          type="number"
          min="1"
          max="1000"
          name="Quantity"
          onChange={numChange}
        />
      </div>
      <div>
        <p>Select Item Type</p>
        <select>
          <option>Non Perishable Food</option>
          <option>Non Food Item</option>
          <option>Other</option>
        </select>
      </div>
    </Fragment>
  )
}

// const donationEntryController = (model) => {
//   model.update;
// };

// const DonationModel = () => {
//   let callBacks = [];
//   return {
//     updateInventory: function () {
//       fetch(`https://jsonplaceholder.typicode.com/users`)
//         .then((response) => response.json())
//         .then((info) => callBacks.forEach((c) => c(info)));
//     },
//     subscribe: function (setInventoryData) {
//       callBacks.push(setInventoryData);
//     },
//   };
// };

const DonationEntry = () => {
  const [itemData, updateFormData] = useState([])
  const [numData, updateNumData] = useState([])
  const [donorData, updateDonorData] = useState("")

  const [finalData, updateFinalData] = useState({})

  const handleChange = e => {
    updateFormData(itemData.concat(e.target.value))
    console.log("changing")
  }

  const handleNumChange = e => {
    updateNumData(numData.concat(e.target.value))
    console.log("changing number")
  }

  const handleIdChange = e => {
    updateDonorData(e.target.value)
    console.log("changing donor")
  }

  const buildItems = () =>
    itemData.map((value, index) => [
      { itemName: itemData[index] },
      { quantity: numData[index] },
    ])

  const handleSubmit = e => {
    const data = donorData
    const items = buildItems()

    e.preventDefault()
    updateFinalData({
      donorId: data,
      items: items,
    })

    console.log(finalData)
  }

  const [form, setFormState] = useState([
    <FormItem
      key={Math.random()}
      change={handleChange}
      numChange={handleNumChange}
    />,
  ])

  const HandleAddClick = () => {
    setFormState(
      form.concat(
        <FormItem change={handleChange} numChange={handleNumChange} />
      )
    )
  }

  const noZero = num => (num == 0 ? 1 : num)
  const HandleMinusClick = () => {
    setFormState(form.slice(0, noZero(form.length - 1)))
    updateFormData(itemData.slice(0, noZero(itemData.length - 1))) // i think its item data
  }

  const listItems = form.map((element, index) => (
    <Fragment key={index}>{element}</Fragment>
  ))

  return (
    <DashBoardLayout>
      <h1>Donation Entry Form</h1>
      <div className={styles.inputs}>
        <form onSubmit={handleSubmit}>
          <div className={styles.donorId}>
            <Input type="text" name="Donor ID" onChange={handleIdChange} />
          </div>
          <hr />
          {listItems}
          <div className={styles.submit}>
            <Button color="#990000" textColor="white" text="Submit" />
          </div>
        </form>
        <div className={styles.buttonDiv}>
          <Button
            color="green"
            textColor="white"
            text="Add Item"
            click={HandleAddClick}
          />
          <Button
            color="red"
            textColor="white"
            text="Remove Item"
            click={HandleMinusClick}
          />
        </div>
      </div>
    </DashBoardLayout>
  )
}

export default DonationEntry
