import React from "react"
import { useEffect } from "react"
import { useForm } from "react-hook-form"
import ReactDOM from "react-dom"
import Button from "../../Button/Button"
import Layout from "../../Layout/Layout"
import styles from "./Register.module.scss"
import Input from "../../Input/Input"
import PassFail from "../../PassFail/PassFail"
import { useState } from "react"

import logo from "@/images/seawolves.png"

function validateName(name) {
  if (name.value) {
    return name.value.length < 32 && name.value.length > 0
  } else {
    return false
  }
}

function validateUserName(value) {
  return value.length < 32 && value.length > 2
}
//Must contain a digit 0 - 9
//Must contain at least one lowercase character
//must contain at least one special character
//length between 8 and 20

const notIsEmpty = string => string.length !== 0

const validatePassword = password => {
  const regex = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{6,20})/

  return notIsEmpty(password) && regex.test(password)
}

const handleSubmit = (...isValid) => !isValid.some(b => b === false)

const Register = () => {
  const [firstName, setFirstName] = useState({ value: "", isValid: false })
  const [lastName, setLastName] = useState({ value: "", isValid: false })
  const [userName, setUserName] = useState({ value: "", isValid: false })

  const [pass, setPass] = useState({
    passValue: "",
    confirmValue: "",
    passValid: false,
    confirmValid: false,
  })
  // const [password, setPassword] = useState({ value: "", isValid: false });
  // const [confirmPass, setConfirmPass] = useState({ value: "", isValid: false });

  const validateConfirmPass = (pass, confirm) =>
    pass === confirm && notIsEmpty(confirm)

  const onChange = (setState, validator) => e => {
    const { value } = e.target
    setState({ value, isValid: validator(value) })
  }

  const firstNameOnChange = onChange(setFirstName, notIsEmpty)
  const lastNameOnChange = onChange(setLastName, notIsEmpty)
  const userNameOnChange = onChange(setUserName, validateUserName)

  const passwordOnChange = e => {
    const { value } = e.target
    setPass(prev => ({
      passValue: value,
      passValid: validatePassword(value),

      confirmValue: prev.confirmValue,
      confirmValid: validateConfirmPass(value, prev.confirmValue),
    }))
  }
  const confirmPassOnChange = e => {
    const { value } = e.target
    setPass(prev => ({
      passValue: prev.passValue,
      passValid: prev.passValid, // doesnt validate the pass when confirm changes

      confirmValue: value,
      confirmValid: validateConfirmPass(prev.passValue, value),
    }))
  }
  const formOnSubmit = e => {
    if (handleSubmit(firstName.isValid, pass.passValid, pass.confirmValid)) true
    else e.preventDefault()
  }
  /*
  const passwordOnChange = onChange(setPassword,validatePassword)
  const confirmPassOnChange = onChange(setConfirmPass,validateConfirmPass)
  */

  return (
    <Layout>
      <div className={styles.container}>
        <div className={styles.logo}>
          <img src={logo} />
        </div>
        <div className={styles.login}>
          <div className={styles.loginInfo}>
            <h1>Already have an account?</h1>
            <p>Log in to your account now</p>
            <Button
              text="Log In"
              color="white"
              textColor="black"
              path="/login"
            />
          </div>
        </div>
        <div className={styles.signUpForm}>
          <div className="welcomeText">
            <h2>Welcome to The Stony Brook</h2>
            <p>Food Pantry</p>
          </div>
          <form onSubmit={formOnSubmit}>
            <div className={styles.formContainer}>
              <Input
                type="text"
                name="FirstName"
                placeholder="First Name"
                value={firstName.value}
                onChange={firstNameOnChange}
              />

              <Input
                type="text"
                name="LastName"
                value={lastName.value}
                placeholder="Last Name"
                onChange={lastNameOnChange}
              />

              <Input
                type="text"
                name="Username"
                value={userName.value}
                placeholder="User Name"
                onChange={userNameOnChange}
              />

              <Input type="email" name="Email" placeholder="Email" />

              <Input
                type="password"
                name="Password"
                value={pass.passValue}
                placeholder="Password"
                onChange={passwordOnChange}
              />

              <Input
                type="password"
                value={pass.confirmValue}
                name="ConfirmPassword"
                placeholder="ConfirmPassword"
                onChange={confirmPassOnChange}
              />
            </div>

            <Button
              textColor="white"
              color="#990000"
              text="Sign Up"
              type="submit"
            />
            <div className={styles.errorProperties}>
              <PassFail
                passfail={firstName.isValid}
                failText="Please Enter Your First Name"
                passText="Check!"
              />

              <PassFail
                passfail={lastName.isValid}
                failText="Please Enter Your Last Name"
                passText="Check!"
              />

              <PassFail
                passfail={userName.isValid}
                failText="Username must be at least 2 characters!"
                passText="Check!"
              />
              <PassFail
                passfail={pass.passValid}
                failText="Not secure (At least a number, symbol, capital letter, and 6
                  characters)"
                passText="Secure Password"
              />
              <PassFail
                passfail={pass.confirmValid}
                failText="Password does not match"
                passText="Password Matches"
              />
            </div>
          </form>
        </div>
      </div>
    </Layout>
  )
}

//Make form responsive
//Style button
//Prevent submit if all is true

export default Register
