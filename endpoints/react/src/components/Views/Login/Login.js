import React from "react"

import Button from "../../Button/Button"
import Layout from "../../Layout/Layout"
import styles from "./Login.module.scss"

import Input from "../../Input/Input"
import logo from "@/images/seawolves.png"

function validateName(name) {
  return name.value.length < 32 && name.value.length > 0
}

function validateUserName(name) {
  return name.value.length < 32 && name.value.length > 2
}
//Must contain a digit 0 - 9
//Must contain at least one lowercase character
//must contain at least one special character
//length between 8 and 20
const validatePassword = password => {
  const regex = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{6,20})/

  return regex.test(password)
}

const Login = () => {
  return (
    <Layout>
      <div className={styles.container}>
        <div className={styles.logInForm}>
          <div className={styles.logo}>
            <img src={logo} />
          </div>
          <div className="welcomeText">
            <h2>Welcome to The Stony Brook</h2>
            <p>Food Pantry</p>
          </div>
          <form>
            <Input type="text" name="Username" placeholder="Username" />
            <Input type="password" name="Password" placeholder="password" />
            <div className={styles.checkbox}>
              <p>Remember</p>

              <input type="checkbox" id="remember" name="remember" />
            </div>

            <div className="logInLinks">
              <a href="link">Forgot Password?</a>
            </div>

            <Button
              color="#990000"
              text="Log In"
              textColor="white"
              path="/home"
            />
          </form>
        </div>
        <div className={styles.login}>
          <div className={styles.loginInfo}>
            <h1>No Account? No Problem</h1>
            <p>Create your account now</p>
            <Button
              text="Register"
              color="white"
              textColor="black"
              path="/register"
            />
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default Login
