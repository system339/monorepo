import React, { Fragment } from "react"
import PropTypes from "prop-types"
import styles from "./Home.module.scss"
import DashBoardLayout from "../../DashboardLayout/DashBoardLayout"
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  ResponsiveContainer,
  AreaChart,
  Area,
  BarChart,
  Bar,
} from "recharts"
import logo from "@/images/seawolves.png"
import Swiper from "react-id-swiper"
import { withRouter } from "react-router"

const LowStock = ({ imageSrc, name, amountLeft }) => {
  return (
    <Fragment>
      <img src={imageSrc} />
      <p>{name}</p>
      <p className={styles.red}>{amountLeft} left in stock!</p>
    </Fragment>
  )
}
LowStock.propTypes = {
  imageSrc: PropTypes.string,
  name: PropTypes.string,
  amountLeft: PropTypes.number,
}

const Home = () => {
  const params = {
    slidesPerView: 4,
    spaceBetween: 20,
    freeMode: true,
    slideClass: styles.lowStockItem,
    grabCursor: true,

    scrollbar: {
      el: ".swiper-scrollbar",
      hide: false,
    },
  }

  const data = [
    {
      name: "M",
      success: 7,
      failed: 2,
      amt: 2400,
    },
    {
      name: "T",
      success: 8,
      failed: 1,
      amt: 2210,
    },
    {
      name: "W",
      success: 12,
      failed: 1,
      amt: 2290,
    },
    {
      name: "Th",
      success: 8,
      failed: 3,
      amt: 2000,
    },
    {
      name: "F",
      success: 1,
      failed: 0,
      amt: 2181,
    },
    {
      name: "S",
      success: 4,
      failed: 10,
      amt: 2500,
    },
    {
      name: "Su",
      success: 2,
      failed: 3,
      amt: 2100,
    },
  ]

  const barData = [
    {
      name: "M",
      visitors: 22,
    },
    {
      name: "T",
      visitors: 11,
    },
    {
      name: "W",
      visitors: 14,
    },
    {
      name: "Th",
      visitors: 3,
    },
    {
      name: "F",
      visitors: 23,
    },
    {
      name: "S",
      visitors: 2,
    },
    {
      name: "Su",
      visitors: 3,
    },
  ]

  return (
    <DashBoardLayout>
      <h2>Hi Anne, welcome to your dashboard!</h2>
      <p>Here is what is going on at the pantry lately...</p>
      <div className={styles.charts}>
        <div className={styles.chartitem}>
          <p className={styles.title}>Successful vs Failed Orders</p>
          <ResponsiveContainer>
            <AreaChart data={data}>
              <CartesianGrid strokeDasharray="3 3" />
              <Area
                fill="red"
                type="monotone"
                dataKey="failed"
                stroke="red"
                activeDot={{ r: 8 }}
              />
              <Area
                fill="green"
                type="monotone"
                dataKey="success"
                stroke="green"
              />
              <CartesianGrid stroke="#ccc" />
              <XAxis dataKey="name" />
              <YAxis />
              <Legend />
              <Tooltip />
            </AreaChart>
          </ResponsiveContainer>
        </div>
        <div className={styles.chartitem}>
          <p className={styles.title}>Weekly Visitors</p>
          <ResponsiveContainer>
            <BarChart
              data={barData}
              margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
              }}
              barSize={25}
            >
              <XAxis
                dataKey="name"
                scale="point"
                padding={{ left: 10, right: 10 }}
              />
              <YAxis />
              <Tooltip />
              <CartesianGrid strokeDasharray="3 3" />
              <Bar
                dataKey="visitors"
                fill="#990000"
                background={{ fill: "black" }}
              />
            </BarChart>
          </ResponsiveContainer>
        </div>
      </div>
      <div className={styles.lowStockContainer}>
        <p className={styles.title}>Items Low On Stock</p>
        <Swiper {...params}>
          <div>
            <LowStock name="sardines" amountLeft="3" imageSrc={logo} />
          </div>
          <div>
            <LowStock name="sardines" amountLeft="3" imageSrc={logo} />
          </div>
          <div>
            <LowStock name="sardines" amountLeft="3" imageSrc={logo} />
          </div>
          <div>
            <LowStock name="sardines" amountLeft="3" imageSrc={logo} />
          </div>
          <div>
            <LowStock name="sardines" amountLeft="3" imageSrc={logo} />
          </div>
          <div>
            <LowStock name="sardines" amountLeft="3" imageSrc={logo} />
          </div>
          <div>
            <LowStock name="sardines" amountLeft="3" imageSrc={logo} />
          </div>
          <div>
            <LowStock name="sardines" amountLeft="3" imageSrc={logo} />
          </div>
          <div>
            <LowStock name="sardines" amountLeft="3" imageSrc />
          </div>
        </Swiper>
      </div>
    </DashBoardLayout>
  )
}
export default Home
