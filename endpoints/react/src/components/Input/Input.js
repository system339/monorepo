import React from "react"

import styles from "./Input.module.scss"

const Input = ({ type, name, placeHolder, onChange, min, max }) => {
  return (
    <div className={styles.group}>
      <input
        type={type}
        name={name}
        placeholder={placeHolder}
        onChange={onChange}
        min={min}
        max={max}
        required
      />
      <span className={styles.highlight}></span>
      <span className={styles.bar}></span>
      <label>{name}</label>
    </div>
  )
}

export default Input
