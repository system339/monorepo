import React, { Fragment } from "react"
import logo from "@/images/seawolves.png"
import styles from "./Header.module.scss"
import bell from "@/images/interface.svg"
import downArrow from "@/images/down.svg"
import face from "@/images/face.svg"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faSignOutAlt, faCog } from "@fortawesome/free-solid-svg-icons"

import { slide as Menu } from "react-burger-menu"
import { BrowserRouter as Router, NavLink, Link } from "react-router-dom"

const Header = () => {
  const [toggleState, setToggleState] = React.useState(false)

  function toggle() {
    toggleState ? setToggleState(false) : setToggleState(true)
    // setToggleState(toggleState === false ? styles.on : styles.off);
  }

  return (
    <Fragment>
      <div id="nav" className={styles.header} outerContainerId={"App"}>
        <Link to="/home">
          <div className={styles.logoandtitle}>
            <div className={styles.logo}>
              <img src={logo} />
            </div>
          </div>
        </Link>
        <div className={styles.userProfile}>
          <img className={styles.svgImg} src={bell} />
          <img className={styles.svgImg} src={face} />
          <p className={styles.text}>Ann Brown</p>
          <img className={styles.arrImg} src={downArrow} onClick={toggle} />
          <div className={`popup ${toggleState ? styles.on : styles.off}`}>
            <div className={styles.item}>
              <p>Settings</p>
              <FontAwesomeIcon icon={faCog} />
            </div>
            <div className={styles.item}></div>
            <div className={styles.item}>
              <p>Log Out</p>
              <FontAwesomeIcon icon={faSignOutAlt} />
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default Header
