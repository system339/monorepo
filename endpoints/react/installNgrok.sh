if [[ -f "/usr/bin/local/ngrok" ]]; then
  echo "You already have ngrok installed"
else
  wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip
  unzip ./ngrok-stable-linux-amd64.zip
  chmod +x ngrok
  sudo mv ./ngrok /usr/local/bin
  sudo rm ./ngrok-stable-linux-amd64.zip
  echo "You can now use ngrok globally!"
  echo "Please provide an authtoken to ngrok by running:"
  echo "ngrok authtoken \$TOKEN_GOES_HERE"
  echo "Then you can run"
  echo "npm run share"
fi
