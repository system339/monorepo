<?php

interface Station {
    public function handle($req);
}

/**
 * Class Controller
 *
 * The controller handles events
 * It just converts event data into useCase request
 */
class Controller implements Station {

    private $useCase;

    public function __construct($useCase)
    {
        $this->useCase = $useCase;
    }

    public function handle($req)
    {
        //...transformation of $req
//        $useCaseReq = new UseCaseRequest
        $this->useCase->handle($req);
    }
}

/**
 * Class UseCase
 *
 * Converts UseCaseRequest -> UseCaseResponse
 * these could be rich in logic
 *
 */
class UseCase implements Station {

    private $presenter;

    public function __construct($presenter)
    {
        $this->presenter = $presenter;
    }

    public function handle($req)
    {
        $this->presenter->handle($req);
    }
}

/**
 * Class Presenter
 * takes useCaseResponse -> viewModel
 */
class Presenter implements Station {

    private $view;

    public function __construct($view)
    {
        $this->view = $view;
    }

    public function handle($viewModel)
    {
        $this->view->handle($viewModel);
    }
}

class SpecialComp {
    private $howMany;

    public function __construct($howMany)
    {
        $this->howMany = $howMany;
    }

    public function __invoke()
    {
        return <<<HTML
           <div>
            {4*5}
            </div> 
        HTML;
    }
}

class HTMLView implements Station
{
    public function handle($viewModel)
    {
        $template = <<<HTML
            <div>
            {$viewModel["hello"]}
            {(new SpecialComp(5))()}
            </div>
        HTML;
        echo $template;
    }
}

$controller = new Controller(new UseCase(new Presenter(new HTMLView())));

$controller->handle(["hello"=>5]);