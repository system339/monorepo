<?php


namespace App\Domain\UseCases;

use System\Domain\UseCases\UseCase;
use System\Domain\UseCases\OutputPort;

class FakeLogin implements UseCase
{
    private OutputPort $port;

    public function __construct(OutputPort $port)
    {
        $this->port = $port;
    }

    public function execute($req) {
        $this->port->send(["success" => true]);
    }
}
