<?php


namespace App\Domain\InputPorts;


interface InputPort
{
    public function send($args);
}
