<?php


namespace App\Domain\InputPorts;

use System\Domain\UseCases\UseCaseFactory;

class FakeLoginInputPort implements InputPort
{
    private $fakeLogin;

    public function __construct(UseCaseFactory $useCases)
    {
        $this->fakeLogin = $useCases->makeFakeLogin();
    }

    public function send($args)
    {
        $this->fakeLogin->execute([
            "name" => $args["name"],
        ]);
    }
}
