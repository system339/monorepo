<?php


namespace App\Domain\OutputPorts;


use System\Domain\UseCases\OutputPort;

interface OutputPortFactory
{
    public function makeFakeLogin():OutputPort;
}
