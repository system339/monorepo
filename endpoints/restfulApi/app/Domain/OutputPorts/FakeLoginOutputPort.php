<?php


namespace App\Domain\OutputPorts;


use System\Domain\UseCases\OutputPort;

class FakeLoginOutputPort implements OutputPort
{

    private $response;

    public function send($response)
    {
        $this->response = $response;
    }

    public function viewModel() {
        return [
            "name" => "sir",
            "status" => $this->response["success"]?"good":"bad"
        ];
    }
}
