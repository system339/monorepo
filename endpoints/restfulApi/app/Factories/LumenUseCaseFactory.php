<?php

namespace App\Factories;

use App\Domain\OutputPorts\OutputPortFactory;
use App\Domain\UseCases\FakeLogin;
use System\Domain\UseCases\UseCase;
use Illuminate\Contracts\Container\Container;
use System\Domain\UseCases\UseCaseFactory;

class LumenUseCaseFactory implements UseCaseFactory
{
    private $app;

    public function __construct(Container $app)
    {
        $this->app = $app;
        $app->singleton(FakeLogin::class, function ()  use ($app) {
            $out = $app->make(OutputPortFactory::class)->makeFakeLogin();
            return new FakeLogin($out);
        });
    }

    public function makeFakeLogin():UseCase {
        return $this->app->get(FakeLogin::class);
    }
}
