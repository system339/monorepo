<?php

namespace App\Factories;

use App\Domain\OutputPorts\FakeLoginOutputPort;
use App\Domain\OutputPorts\OutputPortFactory;
use System\Domain\UseCases\OutputPort;
use Illuminate\Support\ServiceProvider;

class LumenOutputPortFactory implements OutputPortFactory
{
    private $app;

    public function __construct($app)
    {
        $this->app = $app;
        $app->singleton(FakeLoginOutputPort::class, function ($app) {
            return new FakeLoginOutputPort();
        });
    }

    public function makeFakeLogin():OutputPort
    {
        return $this->app->get(FakeLoginOutputPort::class);
    }
}
