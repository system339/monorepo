<?php


namespace App\Factories;

use App\Domain\InputPorts\FakeLoginInputPort;
use App\Domain\InputPorts\InputPortFactory;
use App\Domain\InputPorts\InputPort;
use Illuminate\Contracts\Container\Container;
use System\Domain\UseCases\UseCaseFactory;


class LumenInputPortFactory implements InputPortFactory
{
    private $app;

    public function __construct(Container $app)
    {
        $this->app=$app;
        $app->singleton(FakeLoginInputPort::class, function ($app) {
            return new FakeLoginInputPort($app->make(UseCaseFactory::class));
        });
    }

    public function makeFakeLogin(): InputPort
    {
        return $this->app->get(FakeLoginInputPort::class);
    }
}
