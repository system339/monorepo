<?php

namespace App\Providers;

use App\Domain\InputPorts\InputPortFactory;
use App\Domain\OutputPorts\OutputPortFactory;
use System\Domain\UseCases\UseCaseFactory;
use App\Factories\LumenInputPortFactory;
use App\Factories\LumenOutputPortFactory;
use App\Factories\LumenUseCaseFactory;
use Illuminate\Support\ServiceProvider;

class FactoryServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(InputPortFactory::class, function ($app) {
            return new LumenInputPortFactory($app);
        });
        $this->app->singleton(OutputPortFactory::class, function ($app) {
            return new LumenOutputPortFactory($app);
        });
        $this->app->singleton(UseCaseFactory::class, function ($app) {
            return new LumenUseCaseFactory($app);
        });
    }
}
