<?php

namespace App\Http\Controllers;

use App\Domain\InputPorts\FakeLoginInputPort;
use App\Domain\InputPorts\InputPortFactory;
use App\Domain\OutputPorts\OutputPortFactory;

class FakeLoginController extends Controller
{
    private $input;
    private $output;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(InputPortFactory $inputs, OutputPortFactory $outputs)
    {

        $this->input = $inputs->makeFakeLogin();
        $this->output = $outputs->makeFakeLogin();
    }

    public function show()
    {
        $this->input->send(["name" => "hi"]);
        $viewModel = $this->output->viewModel();
        return json_encode($viewModel);
    }
}
