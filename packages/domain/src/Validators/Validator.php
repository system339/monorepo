<?php

namespace System\Domain\Validators;

use Ds\Set;

trait Validator
{

    /**
     * @param array $expected
     * @param array|\IteratorAggregate $actual
     * @throws \Exception
     */
    private function throwIfKeysInvalid($expected, $actual): void
    {
        $actual = $this->handleIteratorAggregate($actual);

        $validKeys = new Set(($expected));
        $actualKeys = new Set(array_keys($actual));

        if ($validKeys->diff($actualKeys)->count() !== 0) {
            throw new \Exception("Invalid Keys");
        }
    }

    abstract public function validate($entity);

    /**
     * @param array|\IteratorAggregate $actual
     * @return array
     * @throws \Exception
     */
    private function handleIteratorAggregate($actual): array
    {
        if ($actual instanceof \IteratorAggregate) {
            return (iterator_to_array($actual->getIterator()));
        } else {
            return $actual;
        }
    }
}
