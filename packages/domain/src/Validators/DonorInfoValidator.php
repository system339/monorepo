<?php

namespace System\Domain\Validators;

use Ds\Set;
use foo\bar\Exception;

class DonorInfoValidator
{
    use Validator;

    /**
     * @param array $donor
     * @throws InvalidDonorException
     * @throws \Exception
     */
    public function validate($donor)
    {
        $this->throwIfKeysInvalid(["DonorName", "Email", "Organization/Club"], $donor);

        $validations = [
            "DonorName" => $this->validateName($donor["DonorName"]),
            "Email" => $this->validateEmail($donor["Email"]),
            "Organization/Club" => $this->validateOrganization(
                $donor["Organization/Club"]
            )
        ];

        $invalidKeys = [];
        foreach ($validations as $key => $valid) {
            if (!$valid) {
                $invalidKeys[] = $key;
            }
        }
        if (count($invalidKeys) !== 0) {
            throw new InvalidDonorException($invalidKeys);
        }
    }

    public function validateName($name)
    {
        return !(bool)(\preg_match("/\d+/i", $name));
    }

    public function validateOrganization(string $organization)
    {
        return true;
    }

    public function validateEmail($email)
    {
        $result = filter_var($email, \FILTER_VALIDATE_EMAIL);
        return is_string($result) ? true : $result;
    }
}
