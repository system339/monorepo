<?php

namespace System\Domain\Validators;

class InvalidDonorException extends \Exception
{
    private $invalidAttrs;
    public function __construct($invalidAttrs)
    {
        $this->invalidAttrs = $invalidAttrs;
    }

    public function invalidAttrs()
    {
        return $this->invalidAttrs;
    }
}
