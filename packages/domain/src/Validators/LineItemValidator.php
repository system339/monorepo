<?php

namespace System\Domain\Validators;

class LineItemValidator
{
    use Validator;

    public function validate($lineItem)
    {
        $this->throwIfKeysInvalid(
            [
                "quantity",
                "name",
                "barcode",
                "expiration_date",
                "type",
            ],
            $lineItem
        );

        $validations = [
            "quantity" => $this->validateQuantity($lineItem["quantity"]),
            "name" => $this->validateName($lineItem["name"]),
            "barcode" => $this->validateBarcode($lineItem["barcode"]),
            "expiration_date" => $this->validateExpirationDate($lineItem["expiration_date"]),
            "type" => $this->validateType($lineItem["type"]),
        ];

        foreach ($validations as $key => $validation) {
            if ($validation === false) {
                throw new \Exception("$key is invalid");
            }
        }
    }

    public function validateQuantity($quantity)
    {
        return is_int($quantity) && $quantity !== 0;
    }

    public function validateName($name)
    {
        return true;
    }

    public function validateBarcode($barcode)
    {
        $digits = strlen("$barcode");
        return is_int($barcode) && ($digits >= 12 && $digits <= 14);
    }

    public function validateExpirationDate($date)
    {
        return !date_create($date) ? false : true;
    }

    public function validateType(array $type)
    {
        return $type["type"] === "NonPerishableFood"
            || $type["type"] === "NonFoodItem";
    }
}
