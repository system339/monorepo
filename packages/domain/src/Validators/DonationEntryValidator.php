<?php

namespace System\Domain\Validators;

use Ds\Set;

class DonationEntryValidator
{
    use Validator;

    private $lineItemValidator;

    public function __construct()
    {
        $this->lineItemValidator = new LineItemValidator();
    }

    public function validate($entry)
    {
        $this->throwIfKeysInvalid(["DonorID","Items"], $entry);

        foreach ($entry["Items"] as $i => $item) {
            try {
                $this->lineItemValidator->validate($item);
            } catch (\Exception $e) {
                throw new \Exception("Invalid LineItem at index $i");
            }
        }
    }

    public function validateTimestamp($timestamp)
    {
        return is_int($timestamp);
    }
}
