<?php

namespace System\Domain\UseCases;

interface UseCaseFactory
{
    public function makeFakeLogin(): UseCase;
}
