<?php

namespace System\Domain\UseCases;

interface OutputPort
{
    public function send($response);
}
