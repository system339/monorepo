<?php

namespace System\Domain\UseCases\DonationManagement;

use System\Domain\Repositories\ViewExpectedDonation;
use System\Domain\Repositories\Persistance\PersistanceGateway;
use System\Domain\ResourceNames;

class UpdateDonationEntry
{
    private NewDonationEntry $newDonationEntry;

    private UndoDonationEntry $undoDonationEntry;
    private PersistanceGateway $persister;

    public function __construct($persister)
    {
        $this->persister = $persister;
        $this->newDonationEntry = new NewDonationEntry($persister);
        $this->undoDonationEntry = new UndoDonationEntry($persister);
    }

    public function findByDonationId($donationId)
    {
        $entries = $this->persister->findBy(
            ResourceNames::ENTRY_RESOURCE,
            "*",
            "*"
        );
        return $entries[$donationId];
    }

    public function update($donationId, $lineItemUpdates)
    {
        if (count($lineItemUpdates) == 0) {
            return $donationId; // no changes
        } else {
            $this->undoDonationEntry->undo($donationId);

            $entry = $this->findByDonationId($donationId);
            $items = $entry["Items"];
            for ($i = 0; $i < count($lineItemUpdates); $i++) {
                $lineItemUpdate = $lineItemUpdates[$i];
                $lineItem = $items[$i];
                foreach ($lineItemUpdate as $field => $updatedVal) {
                    $lineItem[$field] = $updatedVal;
                }
                $items[$i] = $lineItem;
            }
            $entry["Items"] = $items;

            return $this->newDonationEntry->save($entry);
        }
    }
}
