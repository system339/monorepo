<?php

namespace System\Domain\UseCases\DonationManagement;

use System\Domain\Repositories\Persistance\PersistanceGateway;
use System\Domain\UseCases\DonationManagement\Requests\DonorInfo;
use System\Domain\Validators\DonorInfoValidator;
use System\Domain\Validators\InvalidDonorException;

class NewDonor
{
    private $repo;
    private $validator;
    private $persister;

    public function __construct($persister)
    {
        $this->validator = new DonorInfoValidator();
        $this->persister = $persister;
    }

    public function findById($id)
    {
        try {
            $h = $this->persister->get("DonorInfo", $id);
            if ($h === null) {
                throw new \Exception("Donor ID not found");
            } else {
                return $h;
            }
        } catch (\Exception $e) {
            throw new \Exception("Donor ID not found");
        }
    }

    /**
     * @throws InvalidDonorException
     */
    public function save(DonorInfo $donor)
    {
        $this->validator->validate($donor->toArray());
        return $this->persister->save("DonorInfo", $donor->toArray());
    }
}
