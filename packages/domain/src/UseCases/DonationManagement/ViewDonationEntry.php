<?php

namespace System\Domain\UseCases\DonationManagement;

use System\Domain\Repositories\Persistance\PersistanceGateway;
use System\Domain\ResourceNames;

class ViewDonationEntry
{
    private PersistanceGateway $persister;
    private string $resourceName;

    public function __construct($persister)
    {
        $this->persister = $persister;
        $this->resourceName = ResourceNames::ENTRY_RESOURCE;
    }

    private function findById($donorId)
    {
        try {
            $donationEntry = $this->persister->get($this->resourceName, $donorId);
        } catch (\Exception $e) {
            throw new \Exception("Donor ID not found");
        }
        if ($donationEntry === null) {
            throw new \Exception("Donor ID not found");
        } else {
            return $donationEntry;
        }
    }

    private function findAll($donorId)
    {
        return $this->persister->findBy($this->resourceName, "DonorID", $donorId);
    }

    public function view($donationEntryId)
    {
        return $this->findById($donationEntryId);
    }

    public function viewAll($donorID)
    {
        return $this->findAll($donorID);
    }
}
