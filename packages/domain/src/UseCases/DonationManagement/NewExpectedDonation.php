<?php

namespace System\Domain\UseCases\DonationManagement;

use System\Domain\Repositories\Persistance\PersistanceGateway;
use System\Domain\ResourceNames;

class NewExpectedDonation
{
    private ViewExpectedDonation $donationEntryRepo;

    private NewDonationEntry $newDonationEntry;

    private PersistanceGateway $persister;

    public function __construct($persister)
    {
        $this->persister = $persister;
        $this->newDonationEntry = new NewDonationEntry($persister);
    }

    public function saveAsExpected(array $entry)
    {
        $copy = array_map(fn ($c) => $c, $entry);
        $copy["Created_at"] = \time();
        $id = $this->persister->save(ResourceNames::EXPECTED_ENTRY_RESOURCE, $copy);
        return [
            "id" => $id,
            "Created_at" => $this->persister->get(ResourceNames::EXPECTED_ENTRY_RESOURCE, $id)["Created_at"]
        ];
    }

    public function save($entry)
    {
        return $this->saveAsExpected($entry);
    }

    public function findExpectedById($donationId)
    {
        $entries = $this->persister->findBy(
            ResourceNames::EXPECTED_ENTRY_RESOURCE,
            "*",
            "*"
        );
        return $entries[$donationId];
    }

    public function markAsNewDonation($expectedId)
    {
        $expectedEntries = $this->findExpectedById($expectedId);
        return $this->newDonationEntry->save($expectedEntries);
    }
}
