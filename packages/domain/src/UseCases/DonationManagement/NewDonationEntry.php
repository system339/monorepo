<?php

namespace System\Domain\UseCases\DonationManagement;

use System\Domain\Repositories\Persistance\PersistanceGateway;
use System\Domain\ResourceNames;
use System\Domain\Validators\DonationEntryValidator;

use function time;

class NewDonationEntry
{
    private const ENTRY_RESOURCE = ResourceNames::ENTRY_RESOURCE;
    private $donationEntryRepo;
    private DonationEntryValidator $validator;
    private PersistanceGateway $persister;
    
    public function __construct($persister)
    {
        $this->persister = $persister;
        $this->validator = new DonationEntryValidator();
    }

    public function makeSureDonorExists($id)
    {
        $donorInfo = null;
        try {
            $donorInfo = $this->persister->get("DonorInfo", $id);
        } catch (\Exception $e) {
            throw new \Exception("Donor ID not found");
        }
        if ($donorInfo === null) {
            throw new \Exception("Donor ID not found");
        }
    }

    private function saveEntry(array $entry)
    {
        $copy = $entry;
        $copy["Created_at"] = time();
        $entryID = $this->persister->save(self::ENTRY_RESOURCE, $copy);
        return [
            "id" => $entryID,
            "Created_at" => $copy["Created_at"]
        ];
    }

    public function save($entry)
    {
        $this->validator->validate($entry);
        
        $this->makeSureDonorExists($entry["DonorID"]);

        if ($entry instanceof \IteratorAggregate) {
            $entry = iterator_to_array($entry->getIterator());
        }
        return $this->saveEntry($entry);
    }
}
