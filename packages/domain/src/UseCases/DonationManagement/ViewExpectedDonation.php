<?php

namespace System\Domain\UseCases\DonationManagement;

use System\Domain\Repositories\Persistance\PersistanceGateway;
use System\Domain\ResourceNames;

class ViewExpectedDonation
{
    private PersistanceGateway $persistence;

    public function __construct(PersistanceGateway $persistence)
    {
        $this->persistence = $persistence;
    }

    public function viewExpectedById($donationId)
    {
        $entries = $this->persistence->findBy(
            ResourceNames::EXPECTED_ENTRY_RESOURCE,
            "*",
            "*"
        );
        return $entries[$donationId];
    }
}
