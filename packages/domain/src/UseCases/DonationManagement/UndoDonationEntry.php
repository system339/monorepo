<?php

namespace System\Domain\UseCases\DonationManagement;

use System\Domain\Repositories\ViewExpectedDonation;
use System\Domain\Repositories\Persistance\PersistanceGateway;

class UndoDonationEntry
{
    private const ENTRY_RESOURCE = "DonationEntry";
    private PersistanceGateway $persister;
    
    private $newDonationEntry;


    public function __construct($persister)
    {
        $this->newDonationEntry = new NewDonationEntry(
            $persister
        );
        $this->persister = $persister;
    }

    public function findById($donorId)
    {
        $donorEntries = null;
        try {
            $donorEntries = $this->persister->get(self::ENTRY_RESOURCE, $donorId);
        } catch (\Exception $e) {
            throw new \Exception("Donor ID not found");
        }
        if ($donorEntries === null) {
            throw new \Exception("Donor ID not found");
        }
        return $donorEntries;
    }

    public function undo($entryId)
    {
        $entry = $this->findById($entryId);

        $items = $entry["Items"];

        foreach ($items as $key => $item) {
            $entry["Items"][$key]["quantity"] = -$item["quantity"];
        }
        
        return $this->newDonationEntry->save($entry)["id"];
    }
}
