<?php

namespace System\Domain\UseCases\DonationManagement\Requests;

use System\Domain\Validators\DonorInfoValidator;

class DonorInfo extends Arrayable
{
    public $name;
    public function __construct($info)
    {
        (new DonorInfoValidator())->validate($info);
        $this->container = $info;
        $this->name = $info["DonorName"];
    }
}
