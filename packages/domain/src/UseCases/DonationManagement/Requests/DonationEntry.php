<?php

namespace System\Domain\UseCases\DonationManagement\Requests;

use Exception;
use Traversable;

class DonationEntry extends Arrayable
{
    public $donorId;
    public array $items;

    public function __construct(array $entry)
    {
        $this->donorId = $entry["DonorID"];
        $this->items = $entry["Items"];
        $this->container = $entry;
    }
}
