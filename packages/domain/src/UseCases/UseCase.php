<?php

namespace System\Domain\UseCases;

interface UseCase
{
    public function execute($request);
}
