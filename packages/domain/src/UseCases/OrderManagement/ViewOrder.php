<?php

namespace System\Domain\UseCases\OrderManagement;

use System\Domain\Repositories\Persistance\PersistanceGateway;
use System\Domain\ResourceNames;

class ViewOrder
{
    private PersistanceGateway $persister;

    public function __construct($persister)
    {
        $this->persister = $persister;
    }

    public function findByReceipt(string $receiptCode): array
    {
        return $this->persister->get(ResourceNames::ORDER_RESOURCE_NAME, $receiptCode);
    }

    public function findAll()
    {
        return $this->persister->findBy(ResourceNames::ORDER_RESOURCE_NAME, "*", "*");
    }
}
