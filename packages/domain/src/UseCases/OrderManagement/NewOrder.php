<?php

namespace System\Domain\UseCases\OrderManagement;

use System\Domain\Repositories\Persistance\PersistanceGateway;
use System\Domain\ResourceNames;

class NewOrder
{
    private PersistanceGateway $persister;

    public function __construct($persister)
    {
        $this->persister = $persister;
    }

    public function saveWithTimestamp($order, $timestamp)
    {
        $orderCopy = $order;
        $orderCopy["Created_at"] = $timestamp;
        $receipt = $this->persister->save(ResourceNames::ORDER_RESOURCE_NAME, $orderCopy);
        return $receipt;
    }

    public function save($order)
    {
        return $this->saveWithTimestamp($order, date("m-d-Y"));
    }
}
