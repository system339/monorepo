<?php

namespace System\Domain\UseCases\OrderManagement;

use System\Domain\Repositories\Persistance\PersistanceGateway;
use System\Domain\ResourceNames;

class CallAheadOrder
{
    private ViewCallAheadOrder $viewCallAheadOrder;
    private NewOrder $newOrder;
    private PersistanceGateway $persister;

    public function __construct($persister)
    {
        $this->persister = $persister;
        $this->viewCallAheadOrder = new ViewCallAheadOrder($this->persister);
        $this->newOrder = new NewOrder($persister);
    }

    public function save($order)
    {
        return $this->persister->save(ResourceNames::CALL_AHEAD_RESOURCE_NAME, $order);
    }

    public function markAsNewOrder($receipt)
    {
        $order = $this->viewCallAheadOrder->findCallAheadByReceipt($receipt);
        return $this->newOrder->save($order);
    }
}
