<?php

namespace System\Domain\UseCases\OrderManagement;

use System\Domain\Repositories\Persistance\PersistanceGateway;
use System\Domain\ResourceNames;

use function date;

class ViewCallAheadOrder
{
    private PersistanceGateway $persister;

    public function __construct($persister)
    {
        $this->persister = $persister;
    }

    public function findCallAheadByReceipt(string $receiptCode): array
    {
        return $this->persister->get(ResourceNames::CALL_AHEAD_RESOURCE_NAME, $receiptCode);
    }
}
