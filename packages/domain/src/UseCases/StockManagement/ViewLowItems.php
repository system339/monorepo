<?php

namespace System\Domain\UseCases\StockManagement;

use System\Domain\Repositories\Persistance\PersistanceGateway;
use System\Domain\ResourceNames;

class ViewLowItems
{
    private PersistanceGateway $persister;

    public function __construct($persister)
    {
        $this->persister = $persister;
    }

    private function selectDonationEntries($property)
    {
        $entries = $this->persister->findBy("DonationEntry", "DonorID", "*");

        $found = $this->combinePropertyOfAllItems($entries, $property);

        return ($found);
    }

    /**
     * @param array $entries
     * @param string $property
     * @return array
     */
    private function combinePropertyOfAllItems($entries, $property): array
    {
        $found = [];
        foreach ($entries as $entry) {
            foreach ($entry["Items"] as $items) {
                $name = $items["name"];
                $found = $this->setDefault($found, $name);
                $found[$name] = $found[$name] + $items[$property];
            }
        }
        return $found;
    }

    /**
     * @param array $found
     * @param string $name
     * @return array
     */
    private function setDefault(array $found, $name): array
    {
        if (!isset($found[$name])) {
            $found[$name] = 0;
        }
        return $found;
    }

    private function selectOrder($property)
    {
        $orders = $this->persister->findBy(ResourceNames::ORDER_RESOURCE_NAME, "*", "*");

        $found = [];
        foreach ($orders as $lineItems) {
            foreach ($lineItems as $id => $lineItem) {
                if ($id === "Created_at") {
                    continue;
                }
                $name = $lineItem["name"];
                $found = $this->setDefault($found, $name);
                $found[$name] = $found[$name] + $lineItem[$property];
            }
        }
        return $found;
    }

    public function view()
    {
        $donationEntries = $this->selectDonationEntries("quantity");
        $orders = $this->selectOrder("quantity");

        $allItems = $this->subtractOrders($donationEntries, $orders);

        $lowItems = array_filter($allItems, fn ($el) => $el < 4);
        return $lowItems;
    }

    /**
     * @param array $donationEntries
     * @param array $orders
     * @return array
     */
    private function subtractOrders($donationEntries, $orders)
    {
        $allItems = $donationEntries; // copies array
        foreach ($donationEntries as $name => $quantity) {
            if (isset($orders[$name])) {
                $allItems[$name] = $allItems[$name] - $orders[$name];
            }
        }
        return $allItems;
    }
}
