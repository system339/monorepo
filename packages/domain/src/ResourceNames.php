<?php

namespace System\Domain;

class ResourceNames
{
    public const ENTRY_RESOURCE = "DonationEntry";
    public const EXPECTED_ENTRY_RESOURCE = "ExpectedDonationEntry";
    public const ORDER_RESOURCE_NAME = "Order";
    public const CALL_AHEAD_RESOURCE_NAME = "CallAheadOrder";
    public const DONOR_INFO_RESOURCE = "DonorInfo";
}
