<?php

namespace System\Domain\Builders;

use System\Domain\Entities\LineItem;

class LineItemBuilder
{
    private $record;

    public function quantity($value)
    {
        if ($this->isNotInteger($value)) {
            throw new \Exception("quantity is not an integer");
        }
        $this->record["quantity"] = $value;
        return $this;
    }

    public function barcode(int $value)
    {
        $this->record["barcode"] = $value;
        return $this;
    }

    public function name(string $value)
    {
        $this->record["name"] = $value;
        return $this;
    }

    public function expirationDate(string $value)
    {
        $this->record["expiration_date"] = $value;
        return $this;
    }

    public function type($value)
    {
        $this->record["type"] = $value;
        return $this;
    }

    public function build()
    {
        return new LineItem(
            $this->record["quantity"],
            $this->record["barcode"],
            $this->record["name"],
            $this->record["expiration_date"],
            $this->record["type"]
        );
    }

    private function isNotInteger($value)
    {
        return !is_integer($value);
    }
}
