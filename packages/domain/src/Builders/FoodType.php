<?php

namespace System\Domain\Builders;

class FoodType
{
    public string $type;
    public string $name;
}
