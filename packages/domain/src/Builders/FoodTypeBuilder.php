<?php

namespace System\Domain\Builders;

class FoodTypeBuilder
{
    private string $type;
    private string $name;

    public function foodType(string $value)
    {
        if ($value !== "NonPerishableFood" && $value !== "NonFoodItem") {
            throw new \Exception("Invalid food type: must be either 'NonPerishableFood' or 'NonFood'");
        }
        $this->type = $value;
        return $this;
    }

    public function name(string $value)
    {
        if ($value === "") {
            throw new FieldRequiredException();
        }
        $this->name = $value;
        return $this;
    }

    public function build()
    {
        $foodType = new FoodType();
        $foodType->type = $this->type;
        $foodType->name = $this->name;
        return $foodType;
    }
}
