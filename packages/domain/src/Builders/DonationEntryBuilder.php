<?php

namespace System\Domain\Builders;

use System\Domain\UseCases\DonationManagement\Requests\DonationEntry;

class DonationEntryBuilder
{
    private $entry;

    public function donorId($value)
    {
        $this->entry["DonorID"] = $value;
        return $this;
    }

    public function items(array $value)
    {
        $this->entry["Items"] = $value;
        return $this;
    }

    public function build()
    {
        return new DonationEntry($this->entry);
    }
}
