<?php

namespace System\Domain\Builders;

use System\Domain\UseCases\DonationManagement\Requests\DonorInfo;

class DonorInfoBuilder
{
    private string $name = "";
    private string $organization = "";
    private string $email = "";

    public function donorName(string $value)
    {
        if ($value === "") {
            throw new FieldRequiredException();
        }
        $this->name = $value;
        return $this;
    }

    public function organization(string $value)
    {
        if ($value === "") {
            throw new FieldRequiredException();
        }
        $this->organization = $value;
        return $this;
    }

    public function email(string $value)
    {
        if ($value === "") {
            throw new FieldRequiredException();
        }
        $this->email = $value;
        return $this;
    }

    public function build()
    {
        if ($this->name === "" || $this->organization === "" || $this->email === "") {
            throw new \Exception("Fields not set");
        }
        return new DonorInfo([
            "DonorName" => $this->name,
            "Organization/Club" => $this->organization,
            "Email" => $this->email
        ]);
    }
}
