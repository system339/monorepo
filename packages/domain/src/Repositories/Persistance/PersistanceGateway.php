<?php

namespace System\Domain\Repositories\Persistance;

interface PersistanceGateway
{
    public function save(string $resource, array $data);
    public function get(string $resource, $id);
    public function findBy(string $resource, string $property, $value);
    public function deleteAll();
    public function close();
}
