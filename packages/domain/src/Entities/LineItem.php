<?php

namespace System\Domain\Entities;

class LineItem
{
    public $quantity;
    public $barcode;
    public $name;
    public $expiration_date;
    public $foodType;

    public function __construct($quantity, $barcode, $name, $expiration_date, $foodType)
    {
        $this->quantity = $quantity;
        $this->barcode = $barcode;
        $this->name = $name;
        $this->expiration_date = $expiration_date;
        $this->foodType = $foodType;
    }
}
