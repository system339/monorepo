<?php

namespace System\Persistance;

use System\Domain\Repositories\Persistance\PersistanceGateway;

class JSONFlatFile implements PersistanceGateway
{
    private string $directory;
    private string $filePath;

    private InMemory $inMemory;

    public function __construct($directory)
    {
        $this->directory = $directory;
        if (!is_dir($directory)) {
            \mkdir($directory);
        }
        $this->filePath = $directory . "database.json";
        $this->inMemory = new InMemory();
    }

    public function save(string $resource, array $data)
    {
        $id = $this->inMemory->save($resource, $data);
        $this->persistInMemoryData();
        return $id;
    }

    public function get(string $resource, $id)
    {
        $this->loadDatabaseContents();
        $h = $this->inMemory->get($resource, $id);
        return $h;
    }

    public function findBy(string $resource, string $property, $value)
    {
        $this->loadDatabaseContents();
        return $this->inMemory->findBy($resource, $property, $value);
    }

    public function deleteAll()
    {
        file_put_contents($this->filePath, "");
        $this->inMemory->deleteAll();
    }

    public function close()
    {
        $this->inMemory->close();
    }

    private function persistInMemoryData(): void
    {
        $newDatabase = json_encode($this->inMemory->getData(), JSON_PRETTY_PRINT);
        file_put_contents($this->filePath, $newDatabase);
    }

    private function loadDatabaseContents()
    {
        $contents = file_get_contents($this->filePath);
        $newDatabase = json_decode($contents, true);
        $this->inMemory->setData($newDatabase);
    }
}
