<?php

namespace System\Persistance;

use System\Domain\Repositories\Persistance\PersistanceGateway;

/**
 * Class InMemory
 *
 * @package  System\Persistance
 */
class InMemory implements PersistanceGateway
{
    private $data = [];

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function initialize($resource)
    {
        if (!isset($this->data[$resource])) {
            $this->data[$resource] = [];
        }
    }

    public function save(string $resource, array $data)
    {
        $this->initialize($resource);
        $id = \uniqid();
        $this->data[$resource][$id] = $data;
        return $id;
    }

    public function get(string $resource, $id)
    {
        $this->initialize($resource);
        return $this->data[$resource][$id];
    }

    public function findBy(string $resource, string $property, $value)
    {
        $this->initialize($resource);
        if ($property === "*") {
            return $this->data[$resource];
        } else {
            return \array_filter(
                $this->data[$resource],
                fn ($doc) => $value === "*" ? true : $doc[$property] === $value
            );
        }
    }

    public function deleteAll()
    {
        $this->data[] = [];
    }

    public function close()
    {
        // does not open any resources
    }
}
